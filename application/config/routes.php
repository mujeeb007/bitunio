<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
require_once( BASEPATH .'database/DB.php' );
$db =& DB();
$query = $db->get( 'giZfInSoOcZeItSiOs');
$admin_dir = $query->row()->admin_url;
$route[$admin_dir] = 'AOdSmIiZn';
$route['default_controller'] = 'Bitunio';
$route['notfound'] = 'bitunio/notfound';
$route["$admin_dir/forgot"] = "AOdSmIiZn/forgot";
$route["$admin_dir/admin_forgetpswd"] = "AOdSmIiZn/admin_forgetpswd";
$route["$admin_dir/pattern"] = "AOdSmIiZn/pattern";
$route["$admin_dir/admin_pattern"] = "AOdSmIiZn/admin_pattern";

$route['profile'] = 'Profile_settings/profile';
$route['profile/(.*)'] = 'Profile_settings/profile/$1';
$route['logout'] = 'bitunio/logout';
$route['bank_verify'] = 'bitunio/bank_verify';
$route['trade/(.*)'] = 'trade/index/$1';
//$route['bitunio/(.*)'] = 'bitunio/index/$1';
$route['refreshsellbuyorders/(.*)/(.*)'] = 'trade/refreshsellbuyorders/$1/$2';
$route['createbuyorder'] = 'trade/createbuyorder';
$route['createsellorder'] = 'trade/createsellorder';
$route['cronmapping'] = 'trade/cronmapping';
$route['mapping'] = 'trade/mapping';
$route['close_active_order/(.*)'] = 'trade/close_active_order/$1';
$route['cancel_stop_order/(.*)'] = 'trade/cancel_stop_order/$1';
$route['deposit/(.*)'] = 'deposit/index/$1';
$route['deposit_ia/(.*)'] = 'deposit/deposit_ia/$1';
$route['deposit_ia'] = 'deposit/deposit_ia';
$route['deposit_ia_submit'] = 'deposit/deposit_ia_submit';

//atm test
$route['deposit_test'] = 'deposit/deposit_test';
//atm test

$route['generate_new_address/(.*)'] = 'deposit/generate_new_address/$1';
$route['refreshdeposit/(.*)'] = 'deposit/refreshdeposit/$1';
$route['refreshwithdraw/(.*)'] = 'withdraw/refreshwithdraw/$1';
$route['refreshwithdraw1/(.*)'] = 'withdraw/refreshwithdraw1/$1';
$route['withdraw/(.*)'] = 'withdraw/index/$1';
$route['coin_tfa'] = 'withdraw/coin_tfa';
$route['withdraw_confirm/(.*)'] = 'withdraw/withdraw_confirm/$1';
$route['withdraw_confirm_admin/(.*)'] = 'AOdSmIiZn/withdraw_confirm_admin/$1';
$route['withdraw_cancel/(.*)'] = 'withdraw/withdraw_cancel/$1';
$route['withdraw_cancel_admin/(.*)'] = 'AOdSmIiZn/withdraw_cancel_admin/$1';
$route['coinwithdrawrequest'] = 'withdraw/coinwithdrawrequest';
$route['withdraw_ia'] = 'withdraw/withdraw_ia';
$route['ewithdraw_confirm/(.*)'] = 'withdraw/ewithdraw_confirm/$1';
$route['ewithdraw_cancel/(.*)'] = 'withdraw/ewithdraw_cancel/$1';
$route['withdraw_ia_submit'] = 'withdraw/withdraw_ia_submit';
$route['withdraw_ia/(.*)'] = 'withdraw/withdraw_ia/$1';
$route['dashboard'] = 'bitunio/dashboard';
$route['transactions'] = 'bitunio/transactions';
$route['refer'] = 'bitunio/refer';



$route['about'] = 'bitunio/about';
$route['terms'] = 'bitunio/terms';
$route['broker_terms'] = 'bitunio/broker_terms';
$route['blogs'] = 'bitunio/blogs';
$route['blogs/(.*)'] = 'bitunio/blogs/$1';
$route['blog_details/(.*)'] = 'bitunio/blog_details/$1';
$route['safe_secure'] = 'bitunio/safe_secure';
$route['testimonials'] = 'bitunio/testimonials';
$route['fees'] = 'bitunio/fees';
$route['faq'] = 'bitunio/faq';
$route['contact'] = 'bitunio/contact';


$route['404_override'] = 'pageHandler';
$route['translate_uri_dashes'] = FALSE;

<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Bitunio extends CI_Controller
	{
	public function __construct()
	{
		parent::__construct();
				error_reporting(E_ERROR);
				$this->load->database();
				$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
				$this->output->set_header("Pragma: no-cache");
				header('X-Frame-Options: SAMEORIGIN');
				header('X-XSS-Protection: 1; mode=block');
				header('X-Content-Type-Options: nosniff');
				$base=base_url();
				header("ALLOW-FROM: $base");
				header("X-Powered-By: $base");

				ini_set('session.gc_maxlifetime',300);
				ini_set('session.cookie_httponly', 1);	
				ini_set('session.use_only_cookies', 1);
				ini_set('session.cookie_secure', 1); 
				
				$this->username=username();
				
			}
			function index()
			{
				 
				 
				$refer_id=$this->uri->segment(3);
				$is_refer=$this->user_model->is_refer($refer_id);
				if($is_refer)
				{
					$refer_id_check=$refer_id;
				}
				else 
				{
					$refer_id_check="";
				}
				
$data['BTC_INR']=$this->user_model->get_trade_summary('BTC','INR');
$data['BTC_AED']=$this->user_model->get_trade_summary('BTC','AED');
$data['ETH_INR']=$this->user_model->get_trade_summary('ETH','INR');
$data['ETH_AED']=$this->user_model->get_trade_summary('ETH','AED');
$data['ETH_BTC']=$this->user_model->get_trade_summary('ETH','BTC');
$data['XRP_BTC']=$this->user_model->get_trade_summary('XRP','BTC');
$data['XMR_BTC']=$this->user_model->get_trade_summary('XMR','BTC');
$data['ETC_BTC']=$this->user_model->get_trade_summary('ETC','BTC');
$data['ZEC_BTC']=$this->user_model->get_trade_summary('ZEC','BTC');
$data['MAID_BTC']=$this->user_model->get_trade_summary('MAID','BTC');
$data['DOGE_BTC']=$this->user_model->get_trade_summary('DOGE','BTC');
$data['DASH_BTC']=$this->user_model->get_trade_summary('DASH','BTC');
$data['SJCX_BTC']=$this->user_model->get_trade_summary('SJCX','BTC');
			
				$data['users_count']=$this->user_model->user_count();
				$data['trans_count']=$this->user_model->trans_count();
				
				$data['exchange']=$this->user_model->get_cms(6);
				$data['order']=$this->user_model->get_cms(7);
				$data['custom']=$this->user_model->get_cms(8);
				$data['security']=$this->user_model->get_cms(9);
				$data['multi']=$this->user_model->get_cms(10);
				$data['bitunio_key']=$this->user_model->get_cms(11);
				$data['sec_key']=$this->user_model->get_cms(12);
				$data['rec_key']=$this->user_model->get_cms(13);
				$data['ref_id']=$refer_id_check;
				$this->load->view("front/index",$data);				
			}
			function register1()
			{
				 
				 
				$type=$this->input->post('type');
				if($type == "individual")
				{
					$check=$this->input->post('check');
					$check1=1;
				}
				else 
				{
					$check=$this->input->post('check');
					$check1=$this->input->post('check1');
				}
				
				if($check != "" && $check1 != "")
					{
						$qry=$this->user_model->email_exist();
						if($qry)
							{
								echo "email";
							}
							else
							{
								$this->user_model->register();
								echo "success";
							}
						
						
					}
					else 
					{
						echo "agree";
					}
			}
	
	function reg_confirm($id)
	{
		
		 
		 
		
		$stat=$this->db->query("select * from SliIaStOeZdIrSeOsu where encrypt_id='$id'")->row();
		
		if($stat->email_check == "no")
		{
			$data['verify_status']='no';
			$qry=$this->user_model->reg_confirm($id);	 
		}
		else 
		{
			$data['verify_status']='yes';
		}
		
		//print_r($data); exit;
		$this->load->view("front/index",$data);
	}
	function login()
			{
				 
				 
				$logged = $this->user_model->login();
				if($logged)
				{
					echo $logged;
				}
				else
				{
					echo "error";
				}
			}

			function forgotpasswords()
 {
 	 
	 
 if($this->security->xss_clean($this->input->post('email')))
	 {
		 $qry=$this->user_model->forgotpasswords();
		 if($qry)
		 {
		 echo "success";
		 }
		 else
		 {
			 echo "error";
		 }
	 }

 }
 
 function forgot1($id)
	{
		// if(log_in())
		// redirect('profile');
		$id=encrypt_decrypt("2",$id);
		//echo $id; exit;
		 
		 
		
		$qry=$this->user_model->forgot_check($id);
		$data['data']=$this->user_model->forgot_check($id);
		$data['em']=encrypt_decrypt("2",$this->db->query("select txid from tneZtInSoOc where 	DiZrIeSsOu='$id'")->row()->txid);
		$date1=$qry->forget_date; 
		if($date1 != "")
		{
		$cur_date 	= date('Y-m-d H:i:s');
		$dispute_time=$date1;		
		$dispute_time = date('Y-m-d H:i:s', strtotime($dispute_time . ' + 1440 minutes')); 
	
		
	//if($cur_date > $dispute_time) right
	if($cur_date > $dispute_time)
	{ 
		$data['status_reset']='expire';
		$this->load->view('front/index',$data);
	}
	else
	{
		$data['status_reset']='present';
		$this->load->view('front/index',$data);		
	}
	 
	}
	else 
	{
		$data['status_reset']='expire';
		$this->load->view('front/index',$data);
	}
 }

 function reset_password()
 {
 	 
		 

	$qry=$this->user_model->reset_pass();
	if($qry)
	{
		echo "success";
	}
	else
	{
		echo "fail";
	}
	 
 }
 
 function logout()
{
	$this->session->sess_destroy();
	redirect('');
}
function notfound()
{
	$this->load->view('front/fnf');
}
function chart_data($pair1, $pair2)
{	
	 
	 
	$interval = 1440;
	$duration = '1m';
	$chartdatas = $this->user_model->get_chartdata($pair1, $pair2, $interval, $duration);	
	die(json_encode($chartdatas));
}
function get_email()
{
	if(!log_in())
	redirect('');
	$this->user_model->get_email();
}
function dashboard()
{
	if(!log_in())
	redirect('');
	$id=user_id();
	$data['details']=$this->user_model->userdetails($id);
	$data['histry']=array_reverse($this->user_model->yroZtIsSiOh1($id));
	
	$data['allcurrency']=$this->user_model->allcurrency();
	
	
	$data['balances']=$this->user_model->get_balances();
	$data['income_his']=$this->user_model->get_income($id);
	$query = $this->user_model->get_data('giZfInSoOcZeItSiOs','','','','','','row'); 
	$data['trade_status']=$query->trade_status;
	$data['exchange_status']=$query->exchange_status;
	$data['type']=user_type();
	$this->load->view('front/dashboard',$data);
}

function bank_verification()
{
	if(!log_in())
	redirect('');
	echo $this->user_model->bank_verification();
}

function bank_verify()
{
	if(!log_in())
	redirect('');
	$data['bank_verification']=$this->user_model->get_bank();
	$this->load->view('front/bank_verify',$data);
}
function subscribe()
{
	 
	 
	echo $this->user_model->subscribe();
}
function about()
{
	 
	 
	$data['pages']=$this->user_model->get_cms(2);	
	$this->load->view('front/about',$data);
}
function terms()
{
	 
	 
	$data['pages']=$this->user_model->get_cms(3);
	$this->load->view('front/terms',$data);
}
function broker_terms()
{
	 
	 
	$data['pages']=$this->user_model->get_cms(5);
	$this->load->view('front/terms',$data);
}
function blogs($cat_id='')
{
	$data['blogs']=array_reverse($this->user_model->get_blogs($cat_id));
	$data['blog_cat']=$this->user_model->blog_cat('active');	
	$this->load->view('front/blogs',$data);
}
function blog_details($blog_id)
{
	 
	
	$data['blog']	  	= $this->user_model->get_blog($blog_id);	
	$data['comments'] 	= $this->user_model->get_comments($blog_id);
	$data['comments_count']	  	= $this->user_model->get_comments_count($blog_id);
	$data['comments_lim']	  	= $this->user_model->get_comments($blog_id,"admin");
	$data['blog_cat']	= $this->user_model->blog_cat('active');	
	$this->load->view('front/blog_details',$data);
}
function safe_secure()
{
	 
	 
	$data['content']=$this->user_model->get_cms(18);
	$this->load->view('front/safe_secure',$data);
}
function testimonials()
{
	 
	 
	$data['testi']=$this->user_model->get_testi();
	$this->load->view('front/testimonials',$data);
}
function fees()
{
	 
	 
	$data['trade_fee']=$this->db->get("egatnZeIcSrOeZpIeSeOf")->result();
	// echo "<pre>";
	// print_r($data); exit;
	$data['fees']=$this->user_model->get_cms(17);
	$this->load->view('front/fees',$data);
}
function faq()
{
	 
	 
	$data['faq']=$this->user_model->faq();
	$this->load->view('front/faq',$data);
}
function contact()
{
	 
	 
	$data['contact']=$this->user_model->get_cms(14);
	$data['site']=$this->db->get('giZfInSoOcZeItSiOs')->row();
	$this->load->view('front/contact',$data);
}
function refer()
{
	 
	 
	if(!log_in())
	redirect('');
	$customer_user_id=$this->session->userdata('user_id');
	$where = "DiZrIeSsOu='".$customer_user_id."'";
	$userdetails=$this->user_model->get_data('SliIaStOeZdIrSeOsu',$where,'','','','','row');
	$data['profile'] =$userdetails;
	$data['type']=user_type();
	$referral_code=$userdetails->refer_id;
	if($referral_code != "")
	{
	
	$data['ref_friends']= $this->db->query("select * from SliIaStOeZdIrSeOsu where refered_id='$referral_code'")->result();
	}
	else 
	{
		$data['ref_friends']="";
	}
	$data['cms']=$this->user_model->get_cms(20);
	$this->load->view('front/refer',$data);
}

function referFriend()
{
	$postdata = $this->input->post();
	$email=$postdata['email'];
	$result=$this->user_model->email_exist(); 
	if($result)
		{
			echo "email";
		}
	else
	{
	$customer_user_id=$this->session->userdata('user_id');
	$where = "DiZrIeSsOu='".$customer_user_id."'";
	$userdeails = $this->user_model->get_data('SliIaStOeZdIrSeOsu',$where,'','','','','row');
	
	$get_email_info	=	$this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='8'")->row();			
	$msg	=	$get_email_info->message;		
	$link=base_url()."bitunio/index/".$userdeails->refer_id .'/'.$userdeails->emanZrIeSsOu;	
	
$msg = str_replace("##FRIENDNAME##",$postdata['friendname'],$msg);	
$msg = str_replace("##MESSAGE##",$postdata['refermessage'],$msg);	
$msg = str_replace("##USERNAME##",$userdeails->emanZrIeSsOu,$msg);	
$msg = str_replace("##LINK##",$link,$msg);	
$msg =str_replace("##COMPANYNAME##",'<b>Bitunio</b>',$msg);

	$this->user_model->mailsettings();
	$this->email->from(admin_email(),company_name());
	$this->email->to($email);
	$this->email->subject("Refer Friend");
	$this->email->message($msg);
	$this->email->send();
	die('success');
		}
}

function blog_comments()
{
	echo $this->user_model->blog_comments_ins();
}

function tfa_confirm()
{
				 
				 

			$user_email1 =$this->security->xss_clean($this->input->post('email'));
			$clientid =$this->security->xss_clean($this->input->post('clientid'));
			
			if($user_email1 != "")
			{
				$user_email=$user_email1;
			}
			else
			{
				$id=$this->db->query("select DiZrIeSsOu from SliIaStOeZdIrSeOsu where client_id='$clientid'")->row()->DiZrIeSsOu;
				$user_email=$this->user_model->get_email_admin($id);
			}
			
			$em=substr($user_email,0,4); 
			$ail=substr($user_email,4); 
		
			$em1=encrypt_decrypt('1',$em);
				
			$quer=$this->db->query("SELECT SliIaStOeZdIrSeOsu.* FROM SliIaStOeZdIrSeOsu INNER JOIN tneZtInSoOc ON tneZtInSoOc.DiZrIeSsOu=SliIaStOeZdIrSeOsu.DiZrIeSsOu where SliIaStOeZdIrSeOsu.dilZiIaSmOe='$ail' AND tneZtInSoOc.txid='$em1'");

				require_once 'GoogleAuthenticator.php';

				$ga = new PHPGangsta_GoogleAuthenticator();
			
				$query1=$quer->row();
				$secret_code=$query1->tfa_code;
				
				$onecode 	= $this->security->xss_clean($this->input->post('onecode'));
				
				$code = $ga->verifyCode($secret_code,$onecode,$discrepancy = 1);
				if($code == 1)
				{
						$login_date	=	date('Y-m-d');
						$login_time	=	date("h:i:s"); 
						$datetime 	= 	$login_date." ".$login_time;
						$login_data['user_id']=$query1->DiZrIeSsOu;			
						$this->session->set_userdata($login_data);
						$user_ip	=	$this->input->ip_address(); 
						$this->load->library('user_agent');
						$user_browser = $this->agent->agent_string();

						//$user_browser	=	$_SERVER['HTTP_USER_AGENT'];
						$historydata = array(
						'DiZrIeSsOu'=>$query1->DiZrIeSsOu,
						'ipAddress'=>$user_ip,
						'Browser'=>$user_browser,
						'Action'=>"Logged in",
						'datetime'=>$datetime
						);
						$this->db->insert('yroZtIsSiOh',$historydata);
						$date=date("d-m-Y H:i:s");
						$id=$this->session->userdata("user_id");
						$data=array("last_login" => $date);
						$this->db->where("DiZrIeSsOu",$id);
						$this->db->update("SliIaStOeZdIrSeOsu",$data);	
						echo "success";
				}
				else 
				{
					echo "false";
				}
}


function tfa_confirm1()
{
	 
	 
	
				require_once 'GoogleAuthenticator.php';
				$ga = new PHPGangsta_GoogleAuthenticator();
				$id=user_id();
				$query1=$this->user_model->userdetails($id);
				$secret_code=$query1->tfa_code;
				
				$onecode 	= $this->security->xss_clean($this->input->post('onecode'));
				
				$code = $ga->verifyCode($secret_code,$onecode,$discrepancy = 1);
				if($code == 1)
				{
					$amount = $this->input->post('amount');
					$currency = $this->input->post('currency');
					$customer_user_id	=  $this->session->userdata('user_id'); 
	if(($customer_user_id=="") || $amount==0 || $amount=="")
	{
	   echo "login";
	}
	else
	{
		$balance = $this->user_model->fetchuserbalancebyId($customer_user_id,$currency);
		if($amount <= $balance)
		{
			echo $result	=	$this->user_model->withdrawcoinrequestmodel();
		}
		else
		{
			echo "balance";
		}
	}
				}
				else 
				{
					echo "false";
				}
}


function tfa_confirm2()
{
				 
				 
	
				require_once 'GoogleAuthenticator.php';
				$ga = new PHPGangsta_GoogleAuthenticator();
				$id=user_id();
				$query1=$this->user_model->userdetails($id);
				$secret_code=$query1->tfa_code;
				
				$onecode 	= $this->security->xss_clean($this->input->post('onecode'));
				
				$code = $ga->verifyCode($secret_code,$onecode,$discrepancy = 1);
				
				if($code == 1)
				{
					$amount = $this->input->post('amount');
					$currency = $this->input->post('wcur');
					$customer_user_id	=  $this->session->userdata('user_id'); 
	if(($customer_user_id	==	"") || $amount	==	0 || $amount	==	"")
	{
	   echo "login";
	}
	else
	{
		
		$balance = $this->user_model->fetchuserbalancebyId($customer_user_id,$currency);
		if($amount <= $balance)
		{
			echo $this->user_model->international_withdraw_req();
		}
		else
		{
			echo "balance";
		}
	}
				}
				else 
				{
					echo "false";
				}
}
function transactions()
{
	 
	 
	if(!log_in())
	redirect('');
	$data['transactions']=$this->user_model->transactions();
	$this->load->view("front/transactions",$data);
}

function bro_status()
{
	
	//echo "sdksdsd"; exit; id_number	
	 
	 
	$currentDate = date('Y-m-d');
	$user_id=user_id();
	
	if($user_id == "")
	{   
		die('Your session expired. Please login again.');
	}
	else
	{
		
	$updatedata = array(
						'bro_status'=>'pending',
						'bro_date'=>date("Y-m-d H:i:s"),
						); 
				//print_r($updatedata); exit;	
						
	$this->db->where("DiZrIeSsOu",$user_id);
	$result = $this->db->update('SliIaStOeZdIrSeOsu',$updatedata);
	
	if($result)
		{
			die('success');
		}
		else
		{
			die('Oops! Your information could not saved');
		}
	}
}

function enquiry()
				{
					

					$mdate=date('Y-m-d');
					$status="no-reply";
					$data=array('username'=>$this->input->post('contact_name'),'email_id'=>$this->input->post('contact_email'),'message'=>$this->input->post('contact_message'),'created_date'=>$mdate,'status'=>$status,'subject'=>$this->input->post('subject')); 
					$res = $this->db->insert('sZuItSrOoZpIpSuOs',$data);  
					if($res)
					{
						echo "success";
					}
					else
					{
						echo "error";
					}
					}
					
function trade_his()
{
	
	$this->user_model->fetchHistory('BTC','INR',true);
}
function cron_api_price()
{
			 
			 
			//BTC_INR
			$get              = file_get_contents("https://finance.google.com/finance/converter?a=1&from=BTC&to=INR");
			$get              = explode("<span class=bld>",$get);
			$get              = explode("</span>",$get[1]);
			$converted_amount = preg_replace("/[^0-9.]/", null, $get[0]);
			$val=$converted_amount * 1;
			$BTC_INR=number_format($val,2, '.','');
				
			$this->user_model->update_api_price('BTC','INR',$BTC_INR);
				
			//BTC_AED
			$get1              = file_get_contents("https://finance.google.com/finance/converter?a=1&from=BTC&to=AED");
			$get1              = explode("<span class=bld>",$get1);
			$get1              = explode("</span>",$get1[1]);
			$converted_amount1 = preg_replace("/[^0-9.]/", null, $get1[0]);
			$val1=$converted_amount1 * 1;
			$BTC_AED=number_format($val1,2, '.','');
				
			$this->user_model->update_api_price('BTC','AED',$BTC_AED);
				
				
				//ETH_BTC
				$get2              = file_get_contents("https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=BTC");
				$res=json_decode($get2,0);
				$ETH_BTC=($res[0]->price_btc);
				$this->user_model->update_api_price('ETH','BTC',$ETH_BTC);
				
				//XRP_BTC
				$get3              = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Ripple/?convert=BTC");
				$res=json_decode($get3,0);
				$XRP_BTC=($res[0]->price_btc);
				$this->user_model->update_api_price('XRP','BTC',$XRP_BTC);
				
				//ETC_BTC
				$get4            = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Ethereum-Classic/?convert=BTC");
				$res=json_decode($get4,0);
				$ETC_BTC=($res[0]->price_btc);
				$this->user_model->update_api_price('ETC','BTC',$ETC_BTC);
				
				//ZEC_BTC
				$get5            = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Zcash/?convert=BTC");
				$res=json_decode($get5,0);
				$ZEC_BTC=($res[0]->price_btc);
				$this->user_model->update_api_price('ZEC','BTC',$ZEC_BTC);
				
				//MAID_BTC
				$get6            = file_get_contents("https://api.coinmarketcap.com/v1/ticker/MaidSafeCoin/?convert=BTC");
				$res=json_decode($get6,0);
				$MAID_BTC=($res[0]->price_btc);
				$this->user_model->update_api_price('MAID','BTC',$MAID_BTC);
				
				 //DOGE_BTC
				$get7            = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Dogecoin/?convert=BTC");
				$res=json_decode($get7,0);
				$DOGE_BTC=($res[0]->price_btc);
				$this->user_model->update_api_price('DOGE','BTC',$DOGE_BTC);
								
				//DASH_BTC
				$get8            = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Dash/?convert=BTC");
				$res=json_decode($get8,0);
				$DASH_BTC=($res[0]->price_btc);
				
				$this->user_model->update_api_price('DASH','BTC',$DASH_BTC);
				
				// //SJCX_BTC
				$get9            = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Storjcoin-X/?convert=BTC");
				$res=json_decode($get9,0);
				$SJCX_BTC=($res[0]->price_btc);
				
				$this->user_model->update_api_price('SJCX','BTC',$SJCX_BTC);
				
				
				$get10           = file_get_contents("https://api.coinmarketcap.com/v1/ticker/Monero/?convert=BTC");
				$res=json_decode($get10,0);
				$XRM_BTC=($res[0]->price_btc);
				
				$this->user_model->update_api_price('XRM','BTC',$XRM_BTC);
}

function cron_offer()
{	

$today = date("m/d/Y");
	$expire = $this->user_model->get_data('giZfInSoOcZeItSiOs','','','','','','row','','offer_expire'); 
	$today_time = strtotime($today);
	$expire_time = strtotime($expire);

		if ($expire_time < $today_time) {
			echo "expired";
		}
		else 
		{
		//echo "not expire";	
		//exit;
	$status='active';
	$where = "status='".$status."'";
	$userdetails=$this->user_model->get_data('SliIaStOeZdIrSeOsu',$where,'','','','','result');
	foreach($userdetails as $userdetails1)
	{
	$email=$this->user_model->get_email_admin($userdetails1->DiZrIeSsOu);	

	$get_email_info	=	$this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='15'")->row();			
	$msg	=	$get_email_info->message;
	$subject	=	$get_email_info->subject;
	$this->user_model->mailsettings();
	$this->email->from(admin_email(),company_name());
	$this->email->to($email);
	$this->email->subject($subject);
	$this->email->message($msg);
	$this->email->send();
	}
		}
}

function cronupdateReceivedamount() // cronjob for deposit
{
	$result = $this->user_model->updateReceivedamount();
	if($result)
	{
		echo "success";
	}
	else
	{
		echo "failure";
	}
}

}

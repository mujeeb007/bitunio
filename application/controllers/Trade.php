<?php
 if (!defined('BASEPATH')) exit('No direct script access allowed');
class Trade extends CI_Controller
		{
			
			public function __construct()
			{
				parent::__construct();

				error_reporting(E_ERROR);
				$this->load->database();		
				$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
				$this->output->set_header("Pragma: no-cache");
				header('X-Frame-Options: SAMEORIGIN'); 
				//header('Access-Control-Allow-Origin: true');
				header('X-XSS-Protection: 1; mode=block');
				header('X-Content-Type-Options: nosniff');
				$base=base_url();
				header("ALLOW-FROM: $base");
				header("X-Powered-By: $base");

				ini_set('session.gc_maxlifetime',300);
				ini_set('session.cookie_httponly', 1);	
				ini_set('session.use_only_cookies', 1);
				ini_set('session.cookie_secure', 1);
				
				// if(!log_in())
				// redirect(''); price 
			$this->username=username();	
			
			$user_newid = log_in();
			if(!$user_newid)
			redirect("");
						
			}
			function index($pair="BTC_INR")
			{

				 
				 				
				if(!log_in())
				redirect('');				
				$val=explode("_",$pair);
				$fc=$val[0];
				$sc=$val[1];
				$check_pair=$this->user_model->chek_pair($fc,$sc);
				
				if($check_pair)
				{
				$data['fee']=$this->user_model->fee_per($pair,"buy");
				$data['sell_price'] =$this->user_model->get_trade_summary($fc,$sc, 'Buy')['maxPrice'];				
				$data['buy_price']= $this->user_model->get_trade_summary($fc,$sc, 'Sell')['minPrice'];
				$data['fc']=$fc;
				$data['sc']=$sc;
				$query = $this->user_model->get_data('giZfInSoOcZeItSiOs','','','','','','row'); 
				$data['trade_status']=$query->trade_status;
				$this->load->view("front/trade",$data);
				}
				else
				{
					redirect("notfound");
				}
			}
			function createbuyorder()
			{
				echo $this->user_model->createbuyorder();
			}
				function createsellorder()
			{
				echo $this->user_model->createsellorder();
			}
			
function mapping()
{

	$result = $this->user_model->mapping(); 
	if($result=="empty")
	{
		echo "failure";
	}
	else
	{
		echo "success_".$result;
		$this->cronmapping();
	}
}
function cronmapping()
	{
		//echo "suren";
		$this->user_model->cron_partial_complete();
		
	}

public function refreshsellbuyorders($first_currency,$second_currency)
	{
		$user_id       = user_id();
		$buy_orders    = $this->user_model->gettradeopenOrders('Buy',$first_currency,$second_currency);
		$sell_orders   = $this->user_model->gettradeopenOrders('Sell',$first_currency,$second_currency);
		$trade_history = array_reverse($this->user_model->fetchHistory($first_currency,$second_currency, true));
		
	
		
		if(empty($sell_orders))
		$sell_orders = array();
		if(empty($buy_orders))
		$buy_orders = array();
		if(empty($trade_history))
		$trade_history = array();
		
		$buy_summary  = $this->user_model->get_trade_summary($first_currency,$second_currency, 'Buy');				
	    $sell_summary = $this->user_model->get_trade_summary($first_currency,$second_currency, 'Sell');	
		$stop_orders = $this->user_model->getmytradestopOrders($first_currency,$second_currency);	


		$return  = array('sell_orders'=>array_values($sell_orders),'buy_orders'=>array_values($buy_orders),'trade_history'=>$trade_history,'buy_summary'=>$buy_summary, 'sell_summary'=>$sell_summary,'stop_orders'=>$stop_orders);
			
		$active_orders = $this->user_model->getmytradeopenOrders($first_currency,$second_currency);
	    if(!$active_orders || empty($active_orders ))
		$active_orders = array();		
		$return['balances'] = $this->user_model->user_balance($user_id);
		$return['active_orders'] = $active_orders;
		//echo "<pre>";print_r($return);exit;
		die(json_encode($return));
	}
	
	function close_active_order($id)
{
	 
	 
	$user_id		= user_id();
	if($user_id=="" )
	{   
		die('error');
	}
	else
	{
		$this->session->set_userdata('sessionCloseorder',$id);
		$res_order = $this->user_model->remove_active_model($id);
		die('success');
	}
}
function cancel_stop_order($id){
	 
	 
 	$where = "trade_id=".$id;
	$query = $this->user_model->get_data('rZeIdSrOoZnIiSoOc',$where,'','','','','row'); 
	if($query)
	{
 		$userId = $query->DiZrIeSsOu;
		$amount = $query->Amount;
		$type = $query->Type;
		$fee = $query->Fee;
		
		$secondcurrency=$query->secondCurrency;
		$firstcurrency=$query->firstCurrency;
		$stoporderprice=$query->stoporderprice;
		
		$tradetradeId 			= $query->trade_id;
		$tradeuserId 			= $query->DiZrIeSsOu;
		$tradePrice 			= $query->Price;
		$tradeAmount 			= $query->Amount;
		$tradeFee 				= $query->Fee;
		$tradeType 				= $query->Type;
		$tradeTotal 			= $query->Total;
		$tradefirstCurrency 	= $query->firstCurrency;
		$tradesecondCurrency 	= $query->secondCurrency;
		$orderDate 				= $query->orderDate;
		$orderTime 				= $query->orderTime;
		$comment = "Cancel order #".$tradetradeId;
		$transactiondata = array
									(
										"DiZrIeSsOu"			=>	$tradeuserId,
										"orderId"			=>	$tradetradeId,
										"type"				=>	$tradeType,
										"currency"			=>	$tradefirstCurrency,
										"secondcurrency"	=>	$tradesecondCurrency,
										"amount"			=>	$tradeAmount,
										"price"				=>	$stoporderprice,
										"total"				=>	$tradeTotal,
										"comment"		=>	$comment,
										"date"				=>	$orderDate,
										"time"				=>	$orderTime,
										"status"			=>	"cancelled"
									);
	$this->db->insert('yrotsiZhInSoOiZtIcSaOsZnIaSrOt',$transactiondata);
		if($type=='Buy')
		{
			$balance = $this->user_model->fetchuserbalancebyId($userId,$secondcurrency);
			$update_bal = $balance+$stoporderprice + $fee;
			$where = "DiZrIeSsOu=".$userId;
			$data1=array($secondcurrency=>$update_bal);
			$res = $this->user_model->update_data('ecnZaIlSaObZrIeSsOuZnIiSoOc',$data1,$where);

			$where2 = "trade_id=".$id;
			$data2=array('status'=>'cancel stop order');
			$res = $this->user_model->update_data('rZeIdSrOoZnIiSoOc',$data2,$where2); 
			$result =  $secondcurrency;
			//return true;
		}
		else
		{
			$balance = $this->user_model->fetchuserbalancebyId($userId,$firstcurrency);
			$update_bal = $balance+$amount;
			$where = "DiZrIeSsOu=".$userId;
			$data1=array($firstcurrency=>$update_bal);
			$res = $this->user_model->update_data('ecnZaIlSaObZrIeSsOuZnIiSoOc',$data1,$where);

			$where2 = "trade_id=".$id;
			$data2=array('status'=>'cancel stop order');
			$res = $this->user_model->update_data('rZeIdSrOoZnIiSoOc',$data2,$where2); 
			$result =  $secondcurrency;
			//return true; BTC
		}
		
	}
	die('success');
}
}

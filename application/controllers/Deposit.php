<?php
 if (!defined('BASEPATH')) exit('No direct script access allowed');
class Deposit extends CI_Controller
		{
			
			public function __construct() 
			{
				parent::__construct();

				error_reporting(E_ERROR);
				$this->load->database();		
				$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
				$this->output->set_header("Pragma: no-cache");
				header('X-Frame-Options: SAMEORIGIN'); 
				//header('Access-Control-Allow-Origin: true');
				header('X-XSS-Protection: 1; mode=block');
				header('X-Content-Type-Options: nosniff');
				$base=base_url();
				header("ALLOW-FROM: $base");
				header("X-Powered-By: $base");

				ini_set('session.gc_maxlifetime',300);
				ini_set('session.cookie_httponly', 1);	
				ini_set('session.use_only_cookies', 1);
				ini_set('session.cookie_secure', 1);

				
				// if(!log_in())
				// redirect(''); price
			$this->username=username();
			
			$user_newid = log_in();
			if(!$user_newid)
			redirect("");				
			}
			function index($cur="BTC")
			{
				$id=user_id();
				$data['verify_status']=verify_status($id); 
				$this->load->view('front/deposit',$data);
			}
			function refreshdeposit($cur)
			{
				$dep_history 	= $this->user_model->get_deposit($cur);
				$address        = $this->user_model->get_address($cur);
				//$address=substr(str_shuffle(str_repeat('abcdefghijklmnopqrstuvwxyz0123456789',5)),0,32);
				//$link=base_url()."generate_newaddress/".$cur;
				if($cur == 'XRP'){
					$result 		   = $this->db->query('SELECT ripple_address as address FROM giZfInSoOcZeItSiOs')->row();	
					$address1 	       = $result->address;
					$image_src = "https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=$address1&choe=UTF-8&chld=L";
					if(empty($dep_history))
					$dep_history = array();
					$return  = array('dep_history'=>$dep_history,'selected_currency'=>$cur,"address"=>$address1.'_'.$address,'link'=>$link,'image_src'=>$image_src);
				}else{
					$image_src = "https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=$address&choe=UTF-8&chld=L";
					if(empty($dep_history))
					$dep_history = array();
					$return  = array('dep_history'=>$dep_history,'selected_currency'=>$cur,"address"=>$address,'link'=>$link,'image_src'=>$image_src);
				}
				die(json_encode($return));
			}
			function generate_new_address($cur)
			{
				$this->user_model->generate_new_address($cur);
			}
			function deposit_ia($cur="INR")
			{
				$id=user_id();
				$data['verify_status']=verify_status($id); 
				
				 
				if($cur == "INR" || $cur == "AED")
				{
				$data['acc_details']=$this->user_model->acc_details($cur);
				$data['deposit_list']=$this->user_model->dep_list($cur);
				$data['fee']=$this->user_model->dep_fee($cur);;
				$data['cur']=$cur;
				$this->load->view('front/deposit_ia',$data);
				}
				else 
				{
					redirect("");
				}
			}
			function deposit_ia_submit()
			{
				echo $this->user_model->deposit_ia_submit();
			}

			

		}			
				

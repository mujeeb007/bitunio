<?php
 if (!defined('BASEPATH')) exit('No direct script access allowed');
class Withdraw extends CI_Controller
		{
			
			public function __construct() 
			{
				parent::__construct();

				error_reporting(E_ERROR);
				$this->load->database();		
				$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
				$this->output->set_header("Pragma: no-cache");
				header('X-Frame-Options: SAMEORIGIN'); 
				//header('Access-Control-Allow-Origin: true');
				header('X-XSS-Protection: 1; mode=block');
				header('X-Content-Type-Options: nosniff');
				$base=base_url();
				header("ALLOW-FROM: $base");
				header("X-Powered-By: $base");

				ini_set('session.gc_maxlifetime',300);
				ini_set('session.cookie_httponly', 1);	
				ini_set('session.use_only_cookies', 1);
				ini_set('session.cookie_secure', 1);
				
				// if(!log_in())
				// redirect(''); price
				// echo "withdraw";
			$this->username=username();
			$user_newid = log_in();
			if(!$user_newid){
			$value=	"Error: you need to be logged in";
			$this->session->set_flashdata('error_w',$value);
			redirect('');
			}
				
			}
			function index($cur="BTC")
			{
				 
				 // echo "withdraw"; exit;
				if(!log_in())
				redirect('');
				 // echo "withdraw1"; exit;
				$id=user_id();
				$data['verify_status']=verify_status($id);
				$data['withdraw_btc']=$this->user_model->get_cms(15);
				$this->load->view('front/withdraw',$data);
			}
			function refreshwithdraw($cur)
			{
				$with_history 	= $this->user_model->get_withdraw($cur);
				$with_fee 	= $this->user_model->with_fee($cur);
				$id=user_id();
				$balance = $this->user_model->fetchuserbalancebyId($id,$cur);
			
				if(empty($with_history))
				$with_history = array();
				$return  = array('with_history'=>$with_history,'selected_currency'=>$cur,'with_fee'=>$with_fee,'balance'=>$balance);
				die(json_encode($return));
			}
			function refreshwithdraw1($cur)
			{
				$with_history 	= $this->user_model->get_withdraw1($cur);	
				if(empty($with_history))
				$with_history = array();
				$return  = array('with_history'=>$with_history);
				die(json_encode($return));
			}			
function coinwithdrawrequest()
{
	 
	 
	$amount = $this->input->post('amount');
	$currency = $this->input->post('currency');
	$customer_user_id        =  $this->session->userdata('user_id'); 
	if(($customer_user_id=="") || $amount==0 || $amount=="")
	{
	   echo "login";
	}
	else
	{
		$balance = $this->user_model->fetchuserbalancebyId($customer_user_id,$currency);
		if($amount <= $balance)
		{
			echo $result	=	$this->user_model->withdrawcoinrequestmodel();
		}
		else
		{
			echo "balance";
		}
	}

}
function withdraw_confirm($token)
{
	$where = "token='".$token."'";
	$row = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc',$where,'','','','','row');
	$Userid 	= 	$row->DiZrIeSsOu;
	$Status 	= 	$row->status;
	$customer_user_id	=	$this->session->userdata('user_id');  
	if($customer_user_id==$Userid)
	{
		
		if($Status=="cancelled" || $Status=="finished" || $Status=="pending1")
		{
			$this->session->set_flashdata('error_w', "Your withdraw request has already been confirmed or cancelled earlier");
			redirect('withdraw');
		}
		else
		{
 			$where = "token='".$token."'";
			$data = array('status'=>"pending1");
			$confirmResult = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc',$data,$where);
			$where = "token='".$token."'";
			$taken = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc',$where,'','','','','row'); 
			$amount 		= 	$taken->amount;
			$purse 			= 	$taken->purse;
			$currency 		= 	$taken->currency;
			$btc_amount 		= $amount;			
			 $where = "token='".$token."'";
				$data = array('trans_id'=>'dummy','status'=>'pending1');
				$result = $this->user_model->update_data('ZwIaSrOdZhItSiOwZnIiSoOc',$data,$where);$where = "token='".$token."'";
				$data = array('status'=>'pending1');
				$result1 = $this->user_model->update_data('yrotsiZhInSoOiZtIcSaOsZnIaSrOt',$data,$where);
				if($result1)
				{
					$confirm	=	base_url()."withdraw_confirm_admin/".$token;	
					$cancel		=	base_url()."withdraw_cancel_admin/".$token;
					$ip			=	$this->input->ip_address();
					$get_email_info	=	$this->db->query("select * from setalpZmIeStOlZiIaSmOe where id='17'")->row();
				$bitunio=company_name();
				$msg	=	$get_email_info->message;			
				$msg =str_replace("##COMPANYNAME##",$bitunio,$msg);
				$msg=str_replace('##AMOUNT##',$amount,$msg);
				$msg=str_replace('##CURRENCY##',$currency,$msg);
				$msg=str_replace('##PURSE##',$purse,$msg);
				$msg=str_replace('##IP##',$ip,$msg);						
				$msg=str_replace('##CONFIRMLINK##',$confirm,$msg);
				$msg=str_replace('##CANCELLINK##',$cancel,$msg);	
				
				
				$this->user_model->mailsettings();
				$this->email->from(admin_email(),company_name());
				$this->email->to(admin_email());
				$this->email->subject("withdraw confirmation");
				$this->email->message($msg);
				$this->email->send();
					$this->session->set_flashdata('success_w', "Your withdraw request confirmed successfully.please wait for admin approval");
					redirect("withdraw/$currency");
				
			}
		}
	}
	else
	{
		$this->session->set_flashdata('error_w', "You need to be logged in to confirm your withdraw request");
		redirect('withdraw');
	}
	
}

function withdraw_cancel($token)
{
	 
	  
	$where = "token='".$token."'";
	$row = $this->user_model->get_data('ZwIaSrOdZhItSiOwZnIiSoOc',$where,'','','','','row');
	$Userid 	= 	$row->DiZrIeSsOu;
	$Status 	= 	$row->status;
	$askamount 	= 	$row->askamount;
	$currency 	= 	$row->currency;
	$or_id 	= 	$row->or_id;
	$customer_user_id	=	$this->session->userdata('user_id');  
	if($customer_user_id==$Userid)
	{

		if($Status=="cancelled" || $Status=="confirmed"  || $Status=="pending1")
		{
			$this->session->set_flashdata('error_w', "Your withdraw request has already been confirmed or cancelled earlier");
		}
		else
		{
			$updated = $this->user_model->updatecancellation($token,$Userid,$currency,$askamount);
			if($updated)
			{
				$data = array('status'=>"cancelled");
				$this->db->where("token",$token);
				$this->db->update("yrotsiZhInSoOiZtIcSaOsZnIaSrOt",$data);
				$this->session->set_flashdata('success_w', "Your withdraw request successfully cancelled.");
			}
		}
	}
	else
	{
		$this->session->set_flashdata('error_w',"You need to be logged in to cancel your withdraw request");
	}
	redirect('withdraw');
}
function withdraw_ia($cur="INR")
{
	 
	  
	if(!log_in())
	redirect('');
	$id=user_id();
	$data['verify_status']=verify_status($id); 
	if($cur == "INR" || $cur == "AED")
	{
	$data['acc_details']=$this->user_model->admin_bank_d();
	$data['cur']=$cur;
	$data['fee']=$this->user_model->with_fee($cur);
	$data['balance']=$this->user_model->fetchuserbalancebyId($id,$cur);;
	$data['with_fee_type']=$this->user_model->with_fee_type($cur);
	$data['bank_details']=$this->user_model->get_bank();
	$data['withdraw_ia']=$this->user_model->get_cms(16);
	$this->load->view('front/withdraw_ia',$data);
	}
	else 
	{
		redirect('');
	}
}

function withdraw_ia_submit()
{	  
	$currency = $this->input->post('wcur');
	$amount = $this->input->post('amount');
	$customer_user_id	=  $this->session->userdata('user_id'); 
	$balance = $this->user_model->fetchuserbalancebyId($customer_user_id,$currency);
	if($amount <= $balance)
		{
			$details=$this->user_model->userdetails($customer_user_id);
			if($details->Tfa_status == "enable")
			{
				echo "enable";
			}
			else 
			{
			echo $this->user_model->international_withdraw_req();
			}
		}
		else
		{
			echo "balance";
		}

}
function ewithdraw_confirm($token)
{
	 
	 
 	$where = "token='".$token."'";
	$row = $this->user_model->get_data('tseZuIqSeOrZwIaSrOdZhItSiOw',$where,'','','','','row');
	
	
	$Userid 	= 	$row->DiZrIeSsOu;  
	$Status 	= 	$row->status;
	$customer_user_id	=	$this->session->userdata('user_id');  
	if($customer_user_id==$Userid)
	{
		if($Status=="cancelled" || $Status=="confirmed" || $Status=="filled"|| $Status=="pending"  || $Status=="accepted" || $Status=="rejected" )
		{
		$this->session->set_flashdata('error_w', "Your withdraw request has already been confirmed or canceled earlier");
			redirect('withdraw_ia');
		}
		else
		{
			$confirmResult 	= $this->user_model->updateEconfirmation($token);
			if($confirmResult==1)
			{
				$value	= "Your withdraw request confirmed successfully.";
			}else{
				$value	=	"Application not validated Some Errors are occured.";
			}
			
			$this->session->set_flashdata('success_w',$value);
			redirect('withdraw_ia');
		}
	}
	else
	{
		$value=	"Error: you need to be logged in";
		$this->session->set_flashdata('error_w',$value);
			redirect('');
	}
	//$this->load->view('front/status',$data);
}


function ewithdraw_cancel($token)
{
	 
	 
 	$where = "token='".$token."'";
	$row = $this->user_model->get_data('tseZuIqSeOrZwIaSrOdZhItSiOw',$where,'','','','','row');
	$Userid 	= 	$row->DiZrIeSsOu; 
	$Status 	= 	$row->status;
	$currency 	= 	$row->currency;
	$askamount 	= 	$row->askamount;
	$customer_user_id	=	$this->session->userdata('user_id');  
	if($customer_user_id==$Userid)
	{
		if($Status=="cancelled" || $Status=="confirmed" || $Status=="filled"|| $Status=="pending" || $Status=="accepted" || $Status=="rejected")
		{
		$this->session->set_flashdata('error_w', "Your withdraw request has already been confirmed or canceled earlier");
		redirect('withdraw_ia');
		}
		else
		{
			$confirmResult 	= $this->user_model->updateEconfirmation1($token,$Userid,$currency,$askamount);
			if($confirmResult==1)
			{
				$value	= "Your withdraw request Cancelled successfully.";
			}else{
				$value	=	"Application not validated Some Errors are occured.";
			}
			
			$this->session->set_flashdata('success_w',$value);
			redirect('withdraw_ia');
			
		}
	}
	else 
	{
		$value=	"Error: you need to be logged in";
		$this->session->set_flashdata('error_w',$value);
			redirect('');
	}
}
function coin_tfa()
{
	$amount = $this->input->post('amount');
	$currency = $this->input->post('currency');
	$customer_user_id        =  $this->session->userdata('user_id'); 
	if(($customer_user_id=="") || $amount==0 || $amount=="")
	{
	   echo "login";
	}
	else
	{
		$balance = $this->user_model->fetchuserbalancebyId($customer_user_id,$currency);
		if($amount <= $balance)
		{
			$details=$this->user_model->userdetails($customer_user_id);
			if($details->Tfa_status == "enable")
			{
				echo "enable";
			}
			else 
			{
			echo $this->user_model->withdrawcoinrequestmodel();
			}
		}	
		
		else
		{
			echo "balance";
		}
	}
}




}

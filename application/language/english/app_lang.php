<?php 
$lang['withdraw']='Withdraw'; 

$lang['Welcome']="Welcome";

$lang['Sign in']="Sign in";

$lang['Sign up']="Sign up";

$lang['Sign out']="Sign out";

$lang['Toggle navigation']="Toggle navigation";

$lang['Home']="Home";

$lang['Account']="Account";

$lang['Buy / Sell Bitcoins']="Buy / Sell Bitcoins";

$lang['Trade View']="Trade View";

$lang['Deposit']="Deposit";

$lang['Copyright 2016,  All Rights Reserved.']="Copyright 2016,  All Rights Reserved.";

$lang['Privacy and Data Protection']="Privacy and Data Protection";

$lang['Terms & Conditions']="Terms & Conditions";

$lang['Order book']="Order book";

$lang['Overview']="Overview";

$lang['News']="News";

$lang['Support']="Support";

$lang['Close']="Close";

$lang['Success!']="Success!";

$lang['Select Country']="Select Country";

$lang['Already Registered ?']="Already Registered ?";

$lang['Login Now']="Login Now";

$lang['Sign IN']="Sign IN";

$lang['Remember me']="Remember me";

$lang['Login']="Login";

$lang['Forgot Password ?']="Forgot Password ?";

$lang["Don't Have an Account ?"]="Don't Have an Account ?";

$lang['Register']="Register";

$lang['Create an account']="Create an account";

$lang['Please check your Email!']="Please check your Email!";

$lang['Reset Password']="Reset Password";

$lang['Submit']="Submit";

$lang['Buy Bitcoins Now']="Buy Bitcoins Now";

$lang['Buy']="Buy";

$lang['Select your currency']="Select your currency";

$lang['Buy Bitcoin']="Buy Bitcoin";

$lang['LATEST NEWS']="LATEST NEWS";

$lang['BTC Box Expands !']="BTC Box Expands !";

$lang['Read more']="Read more";

$lang['BUY BITCOINS INSTANTLY WITH']="BUY BITCOINS INSTANTLY WITH";

$lang['LATEST TWEETS']="LATEST TWEETS";

$lang['Live Trade']="Live Trade";

$lang['Time since']="Time since";

$lang['Amount']="Amount";

$lang['Live Order Book']="Live Order Book";

$lang['Transcation history']="Transcation history";

$lang['ALL']="ALL";

$lang['BUY']="BUY";

$lang['Sell']="Sell";

$lang['Type']="Type";

$lang['Date and Time']="Date and Time";

$lang['BTC amount']="BTC amount";

$lang['Total']="Total";

$lang['BTC price']="BTC price";

$lang['Fee']="Fee";

$lang['security']="security";

$lang['Two-Factor Authentication']="Two-Factor Authentication";

$lang['Authentication Key']="Authentication Key";

$lang['Your authentication key']="Your authentication key";

$lang['6-digit authentication code']="6-digit authentication code";

$lang['Enable 2FA']="Enable 2FA";

$lang['Disable 2FA']="Disable 2FA";

$lang['Email Confirmations']="Email Confirmations";

$lang['Request e-mail confirmation for bitcoin withdrawals']="Request e-mail confirmation for bitcoin withdrawals";

$lang['Request e-mail confirmation for ripple withdrawals']="Request e-mail confirmation for ripple withdrawals";

$lang['Request e-mail confirmation for bank withdrawals']="Request e-mail confirmation for bank withdrawals";

$lang['Save Changes']="Save Changes";

$lang['All types']="All types";

$lang['Open Orders']="Open Orders";

$lang['Cancel All Orders']="Cancel All Orders";

$lang['Action']="Action";

$lang['No active orders at the moment']="No active orders at the moment";

$lang['Settings / Bank Account']="Settings / Bank Account";

$lang['Warning!']="Warning!";

$lang['IBAN / bank account']="IBAN / bank account";

$lang['Bank SWIFT / BIC']="Bank SWIFT / BIC";

$lang['Bank name']="Bank name";

$lang['Bank address']="Bank address";

$lang['Bank postal code']="Bank postal code";

$lang['Bank city']="Bank city";

$lang['Bank country']="Bank country";

$lang['Preferred currency']="Preferred currency";

$lang['select country']="select country";

$lang['Save Settings']="Save Settings";

$lang['Change Password']="Change Password";

$lang['Current Password']="Current Password";

$lang['Repeat New password']="Repeat New password";

$lang['New Password']="New Password";

$lang['Save Changes']="Save Changes";

$lang['Notifications']="Notifications";

$lang['Receive occasional newsletter email']="Receive occasional newsletter email";

$lang['Receive email notification for every bitcoin deposit']="Receive email notification for every bitcoin deposit";

$lang['Receive email notification for every USD deposit']="Receive email notification for every USD deposit";

$lang['Save Changes']="Save Changes";

$lang['Deactivate Account']="Deactivate Account";

$lang['Your Password']="Your Password";

$lang['Are you sure you want to deactivate your account?']="Are you sure you want to deactivate your account?";

$lang['DEACTIVATE ACCOUNT']="DEACTIVATE ACCOUNT";

$lang['Support']="Support";

$lang['Which Type of problems you faced']="Which Type of problems you faced";

$lang['Select category']="Select category";

$lang['Bank Withdrawal']="Bank Withdrawal";

$lang['Bitcoin Withdrawal']="Bitcoin Withdrawal";

$lang['Verification']="Verification";

$lang['Deposit']="Deposit";

$lang['Corporate/Business Verification']="Corporate/Business Verification";

$lang['Know Your Customer Procedure']="Know Your Customer Procedure";

$lang['Other']="Other";

$lang['API']="API";

$lang['Write a Request message']="Write a Request message";

$lang['Attachment']="Attachment";

$lang['Browse']="Browse";

$lang['Support Overview']="Support Overview";

$lang['Send']="Send";

$lang['Ticket message']="Ticket message";

$lang['Verify Account']="Verify Account";

$lang['Customer ID']="Customer ID";

$lang['Status']="Status";

$lang['To verify your account please submit']="To verify your account please submit";

$lang['a valid government issued ID, acceptable high quality images of documents are']="a valid government issued ID, acceptable high quality images of documents are";

$lang['international passport (double page)']="international passport (double page)";

$lang['national ID card (both sides)']="national ID card (both sides)";

$lang["driver's license (both sides)"]="driver's license (both sides)";

$lang['a proof of residency, acceptable scanned images of paper documents are']="a proof of residency, acceptable scanned images of paper documents are";

$lang['bank statement']="bank statement";

$lang['utility bill for utilities consumed at your home address']="utility bill for utilities consumed at your home address";

$lang['tax return, council tax']="tax return, council tax";

$lang['certificate of residency issued by a government or a local government authority']="certificate of residency issued by a government or a local government authority";

$lang['You can also submit other documents to serve as proof of residency such as; government-issued documents, judicial authority-issued documents, documents issued by a public agency / authority, utility service company, or similar regulated service providing companies']="You can also submit other documents to serve as proof of residency such as; government-issued documents, judicial authority-issued documents, documents issued by a public agency / authority, utility service company, or similar regulated service providing companies";

$lang['Notice']="Notice";

$lang['To avoid delays when verifying your account, make sure that your submitted documents are']="To avoid delays when verifying your account, make sure that your submitted documents are";

$lang['VISIBLE']="VISIBLE";

$lang['in their']="in their";

$lang['ENTIRETY']="ENTIRETY";

$lang['(watermarks are permitted)']="(watermarks are permitted)";

$lang['images of']="images of";

$lang['HIGH QUALITY']="HIGH QUALITY";

$lang['(colour images, 300dpi resolution or higher)']="(colour images, 300dpi resolution or higher)";

$lang['valid ID documents, with the expiry date clearly visible']="valid ID documents, with the expiry date clearly visible";

$lang['proof of residency document must be a scanned image of a']="proof of residency document must be a scanned image of a";

$lang['PAPER']="PAPER";

$lang['document']="document";

$lang['proof of residency document must be']="proof of residency document must be";

$lang['ISSUED']="ISSUED";

$lang['within the last']="within the last";

$lang['3 MONTHS']="3 MONTHS";

$lang['and addressed to your']="and addressed to your";

$lang['NAME']="NAME";

$lang['and']="and";

$lang['HOME']="HOME";

$lang['address']="Address";

$lang['NOT ELECTRONIC']="NOT ELECTRONIC";

$lang['bills/statements, online screenshots, mobile phone bills or credit card statements.']="bills/statements, online screenshots, mobile phone bills or credit card statements.";

$lang['Verification form']="Verification form";

$lang['First name']="First name";

$lang['Last name']="Last name";

$lang['Postal code']="Postal code";

$lang['City']="City";

$lang['Country']="Country";

$lang['Select country']="Select country";

$lang['ID document number']="ID document number";

$lang['Birth date']="Birth date";

$lang['ID Document issue date']="ID Document issue date";

$lang['ID Document expiration date']="ID Document expiration date";

$lang['Photo ID document']="Photo ID document";

$lang['Proof of residence']="Proof of residence";

$lang['Back side photo ID document']="Back side photo ID document";

$lang['Submit verification request']="Submit verification request";

$lang['My verification was denied, what now?']="My verification was denied, what now?";

$lang['Please_check']="Please check your email for the reason and re-submit a verification request with the correct documents. If you should need further assistance please sub
mit a support ticket to";

$lang['Login History']="Login History";

$lang['DOWNLOAD']="DOWNLOAD";

$lang['IP Address']="IP Address";

$lang['Browser']="Browser";

$lang['Action']="Action";

$lang['Date and Time']="Date and Time";

$lang['No active orders at the moment']="No active orders at the moment";

$lang['International transfer']="International transfer";

$lang['Bitcoin']="Bitcoin";

$lang['All status']="All status";

$lang['Pending']="Pending";

$lang['Confirmed']="Confirmed";

$lang['Canceled']="Canceled";

$lang['Your Deposit Requests']="Your Deposit Requests";

$lang['Date']="Date";

$lang['Description']="Description";

$lang['Amount']="Amount";

$lang["No Record's Found At The Moment"]="No Record's Found At The Moment";

$lang['IDENTITY VERIFICATION REQUIRED. PLEASE']="IDENTITY VERIFICATION REQUIRED. PLEASE";

$lang['VERIFY YOUR ACCOUNT']="VERIFY YOUR ACCOUNT";

$lang['TO PROCEED']="TO PROCEED";

$lang['international wire transfer']="international wire transfer";

$lang['We need you to provide some basic information, so we can process your deposit as soon as possible.']="We need you to provide some basic information, so we can process your deposit as soon as possible.";

$lang['First Name']="First Name";

$lang['Last Name']="Last Name";

$lang['Amount']="Amount";

$lang['Currency']="Currency";

$lang['Comment (Optional)']="Comment (Optional)";

$lang['Description (Optional)']="Description (Optional)";

$lang['Send Your Bitcoins to this Address']="Send Your Bitcoins to this Address";

$lang['Get New Address']="Get New Address";

$lang['Your deposit addresses']="Your deposit addresses";

$lang['Date and time']="Date and time";

$lang['Address']="Address";

$lang['Alert Message']="Alert Message";

$lang['You cannot generate a new address, without using the old']="You cannot generate a new address, without using the old";

$lang['Withdraw']="Withdraw";

$lang['Your Withdrawal Requests']="Your Withdrawal Requests";

$lang['All types']="All types";

$lang['Date']="Date";

$lang['International Wire Transfer']="International Wire Transfer";

$lang['International withdrawals will be charged with']="International withdrawals will be charged with";

$lang['fee, minimum fee is']="fee, minimum fee is";

$lang['on Company Names end and may incur additional international bank fees. Minimum amount for international withdrawal is $50.00. You have']="on Company Names end and may incur additional international bank fees. Minimum amount for international withdrawal is $50.00. You have";

$lang['available. You always have to insert U.S. dollar amount you wish to withdraw. This amount will be converted to the selected currency, free of charge. Third party withdrawal requests will be rejected.']=" available. You always have to insert U.S. dollar amount you wish to withdraw. This amount will be converted to the selected currency, free of charge. Third party withdrawal requests will be rejected.";

$lang['Beneficiary account name']="Beneficiary account name";

$lang['Currency']="Currency";

$lang['Beneficiary account address']="Beneficiary account address";

$lang['Beneficiary account postal code']="Beneficiary account postal code";

$lang['Beneficiary account postal city']="Beneficiary account postal city";

$lang['Beneficiary account country']="Beneficiary account country";

$lang['IBAN / account number']="IBAN / account number";

$lang['SWIFT / BIC']="SWIFT / BIC";

$lang['Beneficiary bank name']="Beneficiary bank name";

$lang['Beneficiary bank address']="Beneficiary bank address";

$lang['Beneficiary bank postal code']="Beneficiary bank postal code";

$lang['Beneficiary bank postal city']="Beneficiary bank postal city";

$lang['Beneficiary bank country']="Beneficiary bank country";

$lang['Further instructions - if provided, delays are possible due to manual processing']="Further instructions - if provided, delays are possible due to manual processing";

$lang['Save data to my profile for later use']="Save data to my profile for later use";

$lang['Withdraw']="Withdraw";

$lang['Bitcoin Withdrawal']="Bitcoin Withdrawal";

$lang['Transfer bitcoins to your wallet address. You have']="Transfer bitcoins to your wallet address. You have";

$lang['bitcoins available.']="bitcoins available.";

$lang['Destination bitcoin address']="Destination bitcoin address";

$lang['Address label (optional)']="Address label (optional)";

$lang['Total Account Balance']="Total Account Balance";

$lang['Transactions']="Transactions";

$lang['Security']="Security";

$lang['Two Factor Authentication']="Two Factor Authentication";

$lang['Withdrawal e-mail confirmations']="Withdrawal e-mail confirmations";

$lang['Open Order']="Open Order";

$lang['Settings']="Settings";

$lang['Change Password']="Change Password";

$lang['Notifications']="Notifications";

$lang['Support']="Support";

$lang['Verify Account']="Verify Account";

$lang['History']="History";

$lang['Deposit List']="Deposit List";

$lang['International Bank']="International Bank";

$lang['Deposit Currency']="Deposit Currency";

$lang['Withdraw List']="Withdraw List";

$lang['International Bank']="International Bank";

$lang['Account Value']="Account Value";

$lang['BTC Balance']="BTC Balance";

$lang['USD Balance']="USD Balance";

$lang['Balance']="Balance";

$lang['Account Balance']="Account Balance";

$lang['Heading']="Heading";

$lang['Order book']="Order book";

$lang['Bids']="Bids";

$lang['Price']="Price";

$lang['Amount']="Amount";

$lang['Value']="Value";

$lang['Asks']="Asks";

$lang['Overview']="Overview";

$lang['Market cap']="Market cap";

$lang['Total']="Total";

$lang['Time since']="Time since";

$lang['News']="News";

$lang["No Record's Found At The Moment"]="No Record's Found At The Moment";

$lang['Fee']="Fee";

$lang['30 days USD volume']="30 days USD volume";

$lang['Fee rounding']="Fee rounding";

$lang["We kindly ask our users to take note on Coinpax's policy regarding fee calculation. As our fees are calculated to two decimal places, all fees which might exceed this limitation are rounded up. The rounding up is executed in such a way, that the second decimal digit is always one digit value higher than it was before the rounding up. For example; a fee of 0.111 will be charged as 0.12."]="We kindly ask our users to take note on Coinpax's policy regarding fee calculation. As our fees are calculated to two decimal places, all fees which might exceed this limitation are rounded up. The rounding up is executed in such a way, that the second decimal digit is always one digit value higher than it was before the rounding up. For example; a fee of 0.111 will be charged as 0.12.";

$lang['Instant Order']="Instant Order";

$lang['Limit Order']="Limit Order";

$lang['Stop Order']="Stop Order";

$lang['BUY BITCOIN']="BUY BITCOIN";

$lang['AVAILABLE']="AVAILABLE";

$lang['I Want to Spend']="I Want to Spend";

$lang['Subtotal']="Subtotal";

$lang['Fee']="Fee";

$lang['Approx']="Approx";

$lang['to receive']="to receive";

$lang['Buy Bitcoins']="Buy Bitcoins";

$lang['SELL BITCOIN']="SELL BITCOIN";

$lang['I Want to Sell']="I Want to Sell";

$lang['Sell Bitcoins']="Sell Bitcoins";

$lang['Amount to Buy']="Amount to Buy";

$lang['Advanced']="Advanced";

$lang['Buy Price']="Buy Price";

$lang['Sell if executed price']="Sell if executed price";

$lang['Amount to Sell']="Amount to Sell";

$lang['Sell Price']="Sell Price";

$lang['Buy if executed price']="Buy if executed price";

$lang['Amount to Buy in']="Amount to Buy in";

$lang['If price rises to']="If price rises to";

$lang['Trailing stop']="Trailing stop";

$lang['Amount to Sell in']="Amount to Sell in";

$lang['Sell Price falls to']="Sell Price falls to";

$lang['Trailing stop']="Trailing stop";

$lang['Sell Bitcoins']="Sell Bitcoins";

$lang['Top 7 Buyers']="Top 7 Buyers";

$lang['Order Type']="Order Type";

$lang['Date and time']="Date and time";

$lang['Trailing price']="Trailing price";

$lang['Triggering price']="Triggering price";

$lang['Option']="Option";

$lang['Cancel']="Cancel";

$lang['Top 7 Buyers']="Top 7 Buyers";

$lang['top 7 sellers']="top 7 sellers";

$lang['No sell orders at the moment']="No sell orders at the moment";

$lang['Account Overview']="Account Overview";

$lang['Customer ID']="Customer ID";

$lang['Account Balance']="Account Balance";

$lang['Account Value']="Account Value";

$lang['balance']="balance";

$lang['Available for trading']="Available for trading";

$lang['waiting for withdrawl']="waiting for withdrawl";

$lang['in open orders']="in open orders";

$lang['Total USD available for trading']="Total USD available for trading";


$lang['Total CHF available for trading']="Total CHF available for trading";

$lang['Potential Balance']="Potential Balance";

$lang['If all orders closed(USD)']="If all orders closed(USD)";

$lang['If all orders closed']="If all orders closed";

$lang['Excluding sell if executed']="Excluding sell if executed";

$lang['If all orders closed(EUR)']="If all orders closed(EUR)";

$lang['If all orders closed(CAD)']="If all orders closed(CAD)";

$lang['Excluding sell if executed']="Excluding sell if executed";

$lang['If all orders closed(CHF)']="If all orders closed(CHF)";

$lang['Commission rate']="Commission rate";

$lang['30 day USD volume']="30 day USD volume";

$lang['30 day EUR volume']="30 day EUR volume";

$lang['30 day CHF volume']="30 day CHF volume";

$lang['30 day CAD volume']="30 day CAD volume";

$lang['Please Enter a valid number']="Please Enter a valid number";

$lang['Incorrect Amount']="Incorrect Amount";

$lang["You don't have a suffient balance"]="You don't have a suffient balance";

$lang['Please Enter numeric value']="Please Enter numeric value";

$lang['The transaction amount exceeds the available balance by']="The transaction amount exceeds the available balance by";

$lang['Click button to']="Click button to";

$lang['sell']="sell";

$lang['Incorrect Amount or Price']="Incorrect Amount or Price";

$lang['You have only']="You have only";

$lang['Sell executive price must greater than Buy price']="Sell executive price must greater than Buy price";

$lang['available. Check your account balance for details']="available. Check your account balance for details";

$lang['Please Enter numeric value']="Please Enter numeric value";

$lang['Incorrect Amount or Price']="Incorrect Amount or Price";

$lang['Buy executive price must less than Sell price.']="Buy executive price must less than Sell price.";

$lang['You have only']="You have only";

$lang['Insufficient']="Insufficient";

$lang['Need to login to make trade']="Need to login to make trade";

$lang['Price must be above current market price']="Price must be above current market price";

$lang['You have only']="You have only";

$lang['Please Enter numeric value']="Please Enter numeric value";

$lang['Incorrect Amount or Price']="Incorrect Amount or Price";

$lang['Price must be below current market price']="Price must be below current market price";

$lang['Order placed']="Order placed";

$lang['forgot password']="forgot password";

$lang['Pair']="Pair";

$lang['Type']="Type";

$lang['Price']="Price";

$lang['Amount']="Amount";

$lang['Total']="Total";

$lang['Filled']="Filled";

$lang['Status']="Status";

$lang['Actions']="Actions";

$lang['Date']="Date";

$lang['Description']="Description";

$lang['Amount']="Amount";

$lang['Status']="Status";

$lang['Deposit USD']="Deposit USD";

$lang['The Fee is']="The Fee is";

$lang['Deposit EUR']="Deposit EUR";

$lang['Select Payment Method']="Select Payment Method";

$lang['Amount to pay']="Amount to pay";

$lang['Enter your Redeem Code here']="Enter your Redeem Code here";

$lang['Redeem']="Redeem";

$lang['Withdrawal']="Withdrawal";

$lang['USD Withdrawal within 72 hours']="USD Withdrawal within 72 hours";

$lang['Available funds']="Available funds";

$lang['Purse']="Purse";

$lang['Amount to withdrawal']="Amount to withdrawal";

$lang['You will receive']="You will receive";

$lang['No']="No";

$lang['orders at the moment']="orders at the moment";

$lang['No active']="No active";

$lang['orders at the moment']="orders at the moment";

$lang['About Us']="About Us";

$lang['Fee']="Fee";

$lang['News']="News";

$lang['Privacy Policy']="Privacy Policy";

$lang['Bitcoin Rate']="Bitcoin Rate";

$lang['Some errors Occured!']="Some errors Occured!";

$lang['Successfully Deposited!']="Successfully Deposited!";

$lang["Your transaction is cancelled"]="Your transaction is cancelled";

$lang["Your request was successfully submited!"]="Your request was successfully submited!";

$lang["Your request is failed! you dont have a enough balance"]="Your request is failed! you dont have a enough balance";

$lang["Pls Try again"]="Pls Try again";

$lang["Information has been edited successfully"]="Information has been edited successfully";

$lang["Information has been saved successfully"]="Information has been saved successfully";

$lang['Process Successfully completed']='Process Successfully completed';

$lang["You don't have suffient balance!"]="You don't have suffient balance!";

$lang["Your request is failed !"]="Your request is failed !";

$lang["Oops! Your information could not saved"]="Oops! Your information could not saved";

$lang["There is some problem"]="There is some problem";

$lang["Ticket has been Deleted "]="Ticket has been Deleted ";

$lang["Your account has been activated successfully Please Check Your Email and login into Your Account"]="Your account has been activated successfully Please Check Your Email and login into Your Account";

$lang["Your Account Has Been Activated Successfully"]="Your Account Has Been Activated Successfully";

$lang['close'] = "close";

?>

 
  $('#referral_form').validate({
                  rules: {
                    
                    friendname: {
                      required: true,                  
                    },
                    email: {
                      required: true,
                      email:true,
                     
                    },
                                     
                  
                  },
                  submitHandler: function (form) {
					   $("#refer_success").hide();  
					   $("#refer_error").hide();  
                       var data = $('#referral_form').serialize();
                      $.ajax({
                        type:'POST',
                        data:data,
                        url:base_url+'bitunio/referFriend',
                        success:function(output) {
                          var output = output.trim();
                           $('.pageloadingBG, .pageloading').css('display', 'none');
                          if(output=="success")
                          {
							$("#stat_suc").html("Success");
							$("#msg_suc").html("Referral email sent successfully.");
							$('#success-modal').modal('show'); 
                            $('#friendname').val('').focus();                            
                            $('#email').val('');                              
                            $('#refermessage').val('');
                          }
							else if(output == "email")
							{
							
							$("#stat").html("Error");
							$("#msg").html("Email Already Exist try another");
							$('#error-modal').modal('show');	
							}
                          else
                          {
                            $("#refer_error").show();
                            $("#refer_error").html("Unable to send referral email. Please enter valid email address and name."); 
                          }    
                        },
                         beforeSend:function(){                 
                            	$('.pageloadingBG, .pageloading').css('display', '');
                          }
                      });
                      return false;
                  }                     
                });   
				

				
$("#verify_broker_form").validate({
			rules: {				
				agree_broker_terms:{required:true},							
				},
			messages:{
			agree_broker_terms:{required:'<b>Please Agree our terms</b>',},
			},
			submitHandler: function (form) {
                    var data = $('#verify_broker_form').serialize();
                      $.ajax({
                        type:'POST',
                        data:data,
						url:base_url+'bitunio/bro_status',
                        success:function(output) {
						$('.pageloadingBG, .pageloading').css('display', 'none');
	                    
						if(output=="success")
							{
								$("#stat_suc").html("Success");
								$("#msg_suc").html("Your Affiliate request is submited successfully, admin will update the status soon");
								$('#success-modal').modal('show');
								setTimeout(function(){ window.location.href=""; }, 3000);	
								}
							else
							{
								$("#stat").html("Error");
								$("#msg").html(output);
								$('#error-modal').modal('show');
							}
                        },
                        beforeSend:function()
						{
                         $('.pageloadingBG, .pageloading').css('display', '');
                        }
                      });
                      return false;
                  },
			
			});
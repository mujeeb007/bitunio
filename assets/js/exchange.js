function refresh_exchange()
  {
	var cur=$("#f_cur").val();
	
	if(cur == "BTC")
	{
	$("#s_cur").find('[value="INR"]').remove();
	$("#s_cur").find('[value="AED"]').remove();
	$("#s_cur").find('[value="BTC"]').remove();
	$("#s_cur").find('[value="ETH"]').remove();
	$("#s_cur").append('<option value="INR">INR</option>');
	$("#s_cur").append('<option value="AED">AED</option>');
	$("#balance").html(btc_bal);
	$("#fcu").html('BTC');
	}
	else if(cur == "ETH")
	{
	$("#s_cur").find('[value="INR"]').remove();
	$("#s_cur").find('[value="AED"]').remove();
	$("#s_cur").find('[value="BTC"]').remove();
	$("#s_cur").find('[value="ETH"]').remove();	
	$("#s_cur").append('<option value="INR">INR</option>');
	$("#s_cur").append('<option value="AED">AED</option>');
	$("#balance").html(eth_bal);
	$("#fcu").html('ETH');
	}
	else if(cur == "INR")
	{
	$("#s_cur").find('[value="INR"]').remove();
	$("#s_cur").find('[value="AED"]').remove();
	$("#s_cur").find('[value="BTC"]').remove();
	$("#s_cur").find('[value="ETH"]').remove();
	$("#s_cur").append('<option value="BTC">BTC</option>');
	$("#s_cur").append('<option value="ETH">ETH</option>');
	$("#balance").html(inr_bal);
	$("#fcu").html('INR');
	}
	else if(cur == "AED")
	{
	$("#s_cur").find('[value="INR"]').remove();
	$("#s_cur").find('[value="AED"]').remove();
	$("#s_cur").find('[value="BTC"]').remove();
	$("#s_cur").find('[value="ETH"]').remove();
	$("#s_cur").append('<option value="BTC">BTC</option>');
	$("#s_cur").append('<option value="ETH">ETH</option>');	
	$("#balance").html(aed_bal);
	$("#fcu").html('AED');
	}
	
	var cur2=$("#s_cur").val();
	$("#f").html(cur);
	$("#s").html(cur2);
	
	var cur1=$("#f_cur").val();
	var cur2=$("#s_cur").val();
	if(cur1 == "BTC" && cur2 == "INR")
	{
	$("#market_fcur").html(cur1);
	$("#market_scur").html(cur2);	
	$(".market_scur").html(cur2);	
	$("#tot").html(BTC_INR);	
	$("#price").val(BTC_INR);
	$("#amount").val("1");
	var fee=fee_BTC_INR;
	}
	else if(cur1 == "BTC" && cur2 == "AED")
	{
	$("#market_fcur").html(cur1);
	$("#market_scur").html(cur2);
	$(".market_scur").html(cur2);	
	$("#tot").html(BTC_AED);
	$("#price").val(BTC_AED);
	$("#amount").val(1);
	var fee=fee_BTC_AED;
	}
	else if(cur1 == "ETH" && cur2 == "INR")
	{
	$("#market_fcur").html(cur1);
	$("#market_scur").html(cur2);
	$(".market_scur").html(cur2);	
	$("#tot").html(ETH_INR);
	$("#price").val(ETH_INR);
	$("#amount").val(1);
	var fee=fee_ETH_INR;
	}
	else if(cur1 == "INR" && cur2 == "BTC")
	{
	$("#market_fcur").html(cur1);
	$("#market_scur").html(cur2);
	$(".market_scur").html(cur2);	
	var val= 1 / parseFloat(BTC_INR);
	$("#price").val(val.toFixed(8));
	$("#tot").html(val.toFixed(8));
	$("#amount").val(1);
	var fee=fee_INR_BTC;	
	}
	else if(cur1 == "INR" && cur2 == "ETH")
	{
	$("#market_fcur").html(cur1);
	$("#market_scur").html(cur2);
	$(".market_scur").html(cur2);	
	var val= 1 / parseFloat(ETH_INR);
	$("#price").val(val.toFixed(8));
	$("#tot").html(val.toFixed(8));
	$("#amount").val(1);
	var fee=fee_INR_ETH;
	}
	else if(cur1 == "AED" && cur2 == "BTC")
	{
	$("#market_fcur").html(cur1);
	$("#market_scur").html(cur2);
	$(".market_scur").html(cur2);	
	var val= 1 / parseFloat(BTC_AED);
	$("#price").val(val.toFixed(8));
	$("#tot").html(val.toFixed(8));
	$("#amount").val(1);
	var fee=fee_AED_BTC;
	}
	else if(cur1 == "AED" && cur2 == "ETH")
	{
	$("#market_fcur").html(cur1);
	$("#market_scur").html(cur2);
	$(".market_scur").html(cur2);	
	var val= 1 / parseFloat(ETH_AED);
	$("#price").val(val.toFixed(8));
	$("#tot").html(val.toFixed(8));
	$("#amount").val(1);
	var fee=fee_AED_ETH;
	}
	else
	{
	$("#price").val(ETH_AED);
	$("#tot").html(ETH_AED);	
	var fee=fee_ETH_AED;
	
	}
	
	if(cur2 == "BTC" || cur2 == "ETH")
	var fix=8;
	else
	var fix=2;
	var price=$("#price").val();
	var amnt=$("#amount").val();
	var price1=parseFloat(price) * parseFloat(amnt);
	var service=parseFloat(price1) * fee / 100;
	var final=parseFloat(price1) - parseFloat(service);
	
	$("#service_charge").html(service.toFixed(fix));
	$("#final").html(final.toFixed(fix));
	
	$.get(base_url+'exchange/refreshexchange', function(data){
	$('.pageloadingBG, .pageloading').css('display', '');
	
    var ex_history 		 = data.ex_history;
	var ex_history_place = '';
	
	var sec_cur_dec=2;
	var fir_cur_dec=8;
	var add='';
	if(ex_history.length>0)
      {
		  for(var j=0;j<ex_history.length;j++){
		  if(ex_history[j].status == "completed")
		  {
		  var statt="<span style='color:green'>completed</span>";
		  }
		  else if(ex_history[j].status == "cancelled")
		  {
		  var statt="<span style='color:red'>cancelled</span>";
		  }
		  else if(ex_history[j].status == "pending")
		  {
		  var statt="<span style='color:orange'>pending</span>";
		  }
		  else if(ex_history[j].status == "rejected")
		  {
		  var statt="<span style='color:red'>rejected</span>";
		  }
		  else 
		  {
		  var statt="<span style='color:orange'>pending</span>";
		  }
		  add=(ex_history[j].status =="pending")?'<a href="javascript:void(0)" onclick="close_exchange_order(\''+ex_history[j].order_id+'\',this)">Cancel</a>':'---';
          ex_history_place += '<tr><td>'+parseInt(j+1)+'</td><td>'+ex_history[j].f_currency+'-'+ex_history[j].t_currency+'</td><td>'+ex_history[j].amount+'</td><td>'+ex_history[j].fees+'</td><td>'+ex_history[j].price+'</td><td>'+ex_history[j].datetime+'</td><td>'+statt+'</td><td>'+add+'</td></tr>';
        }     
      }
      else
      {
        ex_history_place += '<tr><td colspan="6" class="no-records">No Records available</td></tr>';
      }
	  


      $('.ex_history tbody').html(ex_history_place);
	  $('.pageloadingBG, .pageloading').css('display', 'none');
	 
    },'json');
	
  }
  refresh_exchange();
  
  function change_sec(cur2)
  {  
	$("#s").html(cur2);
	var cur1=$("#f_cur").val();
	if(cur1 == "BTC" && cur2 == "INR")
	{
	$("#price").val(BTC_INR);
	$("#tot").html(BTC_INR);
	$("#amount").val(1);
	var fee=fee_BTC_INR;
	}
	else if(cur1 == "BTC" && cur2 == "AED")
	{
	$("#price").val(BTC_AED);
	$("#tot").html(BTC_AED);
	$("#amount").val(1);
	var fee=fee_BTC_AED;
	}
	else if(cur1 == "ETH" && cur2 == "INR")
	{
	$("#price").val(ETH_INR);
	$("#tot").html(ETH_INR);
	$("#amount").val(1);
	var fee=fee_ETH_INR;
	}
	else if(cur1 == "ETH" && cur2 == "AED")
	{
	$("#price").val(ETH_AED);
	$("#tot").html(ETH_AED);
	$("#amount").val(1);
	var fee=fee_ETH_AED;
	}	
	else if(cur1 == "INR" && cur2 == "BTC")
	{
	var val=1/parseFloat(BTC_INR);
	$("#price").val(val.toFixed(8));
	$("#tot").html(val.toFixed(8));
	$("#amount").val(1);
	var fee=fee_INR_BTC;
	}
	else if(cur1 == "INR" && cur2 == "ETH")
	{
	var val=1/parseFloat(ETH_INR);
	$("#price").val(val.toFixed(8));
	$("#tot").html(val.toFixed(8));
	$("#amount").val(1);
	var fee=fee_INR_ETH;
	}
	else if(cur1 == "AED" && cur2 == "BTC")
	{
	var val=1/parseFloat(BTC_AED);
	$("#price").val(val.toFixed(8));
	$("#tot").html(val.toFixed(8));
	$("#amount").val(1);
	var fee=fee_AED_BTC;
	}
	else if(cur1 == "AED" && cur2 == "ETH")
	{
	var val=1/parseFloat(ETH_AED);
	$("#price").val(val.toFixed(8));
	$("#tot").html(val.toFixed(8));
	$("#amount").val(1);
	var fee=fee_AED_ETH;
	}
	else
	{
	$("#price").val(ETH_AED);
	$("#tot").html(val.toFixed(8));
	$("#amount").val(1);
	var fee=fee_ETH_AED;
	}
	$("#market_fcur").html(cur1);
	$("#market_scur").html(cur2);
	$(".market_scur").html(cur2);	
	
	if(cur2 == "BTC" || cur2 == "ETH")
	var fix=8;
	else
	var fix=2;
	var price=$("#price").val();
	var amnt=$("#amount").val();
	var price1=parseFloat(price) * parseFloat(amnt);
	var service=parseFloat(price1) * fee / 100;
	var final=parseFloat(price1) - parseFloat(service);
	$("#service_charge").html(service.toFixed(fix));
	$("#final").html(final.toFixed(fix));
	//refresh_exchange();
	}
  
  $("#exchange").validate({
			rules: {
				amount:{required:true,number:true},				
				price:{required:true,number:true},			
					  },					
					submitHandler: function(form) {
					var ser=$("#service_charge").html();
					var final=$("#final").html();
           var dataform=$('#exchange').serialize();
		   $.ajax({
                     type:'POST',
                     data:dataform+"&ser="+ser+"&final="+final,
                     url:base_url+'exchange/exchange_submit',
					 beforeSend:function(){
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					 $('.pageloadingBG, .pageloading').css('display', 'none');
					   
					 var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$('#exchange')[0].reset();
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order has been Successfully placed");
						$('#success-modal').modal('show');
						refresh_exchange();
						// window.setTimeout(function(){    
                        // window.location.href ="";   },5000);
						}
						else if(doutput == "balance")
						{
						$('#pwd_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');
						}
						else
                        {
						$('#pwd_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html(doutput);
						$('#error-modal').modal('show');
						}
						}
                 });
        },
			
			});

function change_val()
{
var cur=$("#f_cur").val();
var cur2=$("#s_cur").val();
if(cur2 == "BTC" || cur2 == "ETH")
{
var fix=8;
}
else
{
var fix=2;
}

var amount=$("#amount").val();
var price=$("#price").val();
if(cur == "BTC" && cur2 == "INR")
{
 var pr_val=BTC_INR;
 var fee=fee_BTC_INR;
}
else if(cur == "BTC" && cur2 == "AED")
{
 var pr_val=BTC_AED;
  var fee=fee_BTC_AED;
}
else if(cur == "ETH" && cur2 == "INR")
{
 var pr_val=ETH_INR;
 var fee=fee_ETH_INR;
}
else if(cur == "ETH" && cur2 == "AED")
{
 var pr_val=ETH_AED;
 var fee=fee_ETH_AED;
}
else if(cur == "INR" && cur2 == "BTC")
{
 var pr_val= 1 / BTC_INR;
 var fee=fee_INR_BTC;
}
else if(cur == "AED" && cur2 == "BTC")
{
 var pr_val=1 / BTC_AED;
 var fee=fee_AED_BTC;
}
else if(cur == "INR" && cur2 == "ETH")
{
 var pr_val= 1 / ETH_INR;
 var fee=fee_INR_ETH;
}
else 
{
 var pr_val= 1 / ETH_AED;	
 var fee=fee_AED_ETH;
}

var price1=parseFloat(amount) * parseFloat(pr_val);
if(amount != "")
{
$("#price").val(price1.toFixed(fix));
var service=parseFloat(price1) * fee / 100;
var final=parseFloat(price1) - parseFloat(service);
$("#service_charge").html(service.toFixed(fix));
$("#final").html(final.toFixed(fix));
}
else
{
$("#price").val("0.00");
$("#service_charge").html("0.00");
}
}

function close_exchange_order(id, obj){
	if(confirm('Are you sure you want to delete this?')){
		$(obj).parent().parent().css('opacity', '0.2');
		$.get(base_url+'exchange/close_exchange_order/'+id, function(data){
		var output = data.trim();
		
		if(output == "true")
		{
			$(obj).parent().parent().remove();
			refresh_exchange();
			}
			else 
			{
			alert("error please try again");
			}
		
		});
	}
}
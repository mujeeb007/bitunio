jQuery.validator.addMethod("Passwordconditions", function(value, element) {
    return (/^(?=.*[A-Z])(?=.*\d)[A-Za-z\d$@+$!%*#?&]{8,}$/.test(value));
}, "Password must have minimum 8 characters at least 1 Alphabet in Caps, 1 Number 1 Special character");
$("#register").validate({
			rules: {
				username:{required:true},
				check:{required:true},
				email:{required:true,email:true,},	
				password:{required:true,Passwordconditions:true},	
				cpassword:{required:true,equalTo:"#pwd",},	
							},
			messages: {
				email:{required:'Required',email:'Enter Valid Email Id'},
				
			},
			
			submitHandler: function(form) {
			
           var dataform=$('#register').serialize();
		   
           //mujeeb ur rahman Sajid
		   //var gres = $('#res_recaptcha').val();
		         // if (gres == ''){
                      // if error I post a message in a div
                    //  $( '#captcha_error').html( '<p style="color:red">Please verify you are human</p>' );
					//  return false;
                //  }
				//  else 
				//  {
				    $( '#captcha_error').html('');
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/register1',
					 beforeSend:function(){
						$('.pageloadingBG, .pageloading').css('display', '');
						$('#register_btn').attr('disabled', true);
						},
                     success:function(output) 
                     {
						$( '#captcha_error' ).hide();
						$('.pageloadingBG, .pageloading').css('display', 'none');
						$('#register_loader').hide();
                        var doutput = output.trim();
						
						if(doutput == "success")
                        {
							$('#succc').show();						
							$("#msg_succc").html("You have registered successfully please check your mail and activate the account");
							$('#succc').fadeOut(4000);
							setTimeout(function(){ window.location.href=base_url; }, 4000);						
                         }
						else if(doutput == "email")
                        {
							$('#register_btn').attr('disabled', false);
							$("#stat").html("Error");
							$("#msg_errr").html("Email Already Exist Please try Another");
							$('#errr').show();
							$('#errr').fadeOut(4000);
						}
						else if(doutput == "agree")
                        {
							$('#register_btn').attr('disabled', false);
							$("#stat").html("Error");
							$("#msg_errr").html("Please agree our terms");
							$('#errr').show();
							$('#errr').fadeOut(4000);
						}
						else if(doutput == "captcha_error")
                        {
							$('#register_btn').attr('disabled', false);
							$("#stat").html("Error");
							$("#msg_errr").html("Captcha is incorrect");
							$('#errr').show();
							$('#errr').fadeOut(4000);
						}						
						else
                        {
							$('#register_btn').attr('disabled', false);
							$("#stat").html("Error");
							$("#msg_errr").html("Some errors occured please try again later");
							$('#errr').show();
							$('#errr').fadeOut(4000);
						 }
				 }
                 });
				// }//End of Else
				
         },
		 });
			
			$("#login_form1").validate({
			rules: {
				clientid:{required:true,number:true},	
				password:{required:true,},
			},
			messages: {
				
				password:{required:'Required'},
				clientid:{required:'Required',number:'Please enter valid Client Id'},
			},
			
			submitHandler: function(form) {
           var dataform=$('#login_form1').serialize();
		   var gres = $('#res_signup_recaptcha').val();
		     if (gres == ''){
                      // if error I post a message in a div
                      $( '#captcha_error1').html( '<p style="color:red">Please verify you are human</p>' );
					  return false;
                  }
				  else 
				  {
				    $( '#captcha_error1').html('');
		   
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/login',
					 beforeSend:function(){
					$('.pageloadingBG, .pageloading').css('display', '');
					$('#login_btn').attr('disabled', true);
						},
                     success:function(output) 
                     {
					 $('.pageloadingBG, .pageloading').css('display', 'none');
						var doutput = output.trim();
						var doutput1 = doutput.substring(0, 8);
						
						if(doutput == "success")
                        {
						window.location.href='dashboard';
                        }
						 if(doutput == "success1")
                        {
						window.location.href='profile/verification/1';
                         }
						else if(doutput == "error")
                        {
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login_bro").html("Invalid Client id / password");
						$('#errr_login_bro').show();
						$('#errr_login_bro').fadeOut(4000);
						}
						else if(doutput == "change_date")
                        {
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login_bro").html("Your Password is Expired. To proceed with your account, Kindly reset the password");
						$('#errr_login_bro').show();
						$('#errr_login_bro').fadeOut(4000);
						}
						else if(doutput == "deactive")
                        {						
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login_bro").html("Your account has been deactivated, Please contact support@bitunio.com");
						$('#errr_login_bro').show();
						$('#errr_login_bro').fadeOut(4000);
						}
						else if(doutput == "emailcheck")
                        {
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login_bro").html("Please Check your mail and activate your Account");
						$('#errr_login_bro').show();
						$('#errr_login_bro').fadeOut(4000);						                     
						 }
						else if(doutput == "enable")
                        {
						$('#login').modal('hide');						
						$('#login1').modal('hide');						
						$('#tfa').modal('show');                         
						}
						else 
						{
						if(doutput1 == "You have")
						{
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login_bro").html(doutput);
						$('#errr_login_bro').show();
						$('#errr_login_bro').fadeOut(4000);
						}
						}
                     }
                 });
			 }
				
         },
         
			
			});
			
$("#login_form").validate({
			rules: {
				email:{required:true,email:true},	
				password:{required:true,},
			},
			messages: {
				email:{required:'Required',email:'Enter Valid Email Id'},
				password:{required:'Required'},
			},
			
			submitHandler: function(form) {
           var dataform=$('#login_form').serialize();
		   
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/login',
					 beforeSend:function(){
					$('.pageloadingBG, .pageloading').css('display', '');
					$('#login_btn').attr('disabled', true);
						},
                     success:function(output) 
                     {
					 $('.pageloadingBG, .pageloading').css('display', 'none');
						var doutput = output.trim();
						var doutput1 = doutput.substring(0, 8);
						
						
						if(doutput == "success")
                        {
						window.location.href='dashboard';
                         }
						 if(doutput == "success1")
                        {
						window.location.href='profile/verification/1';
                         }
						else if(doutput == "error")
                        {
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login").html("Invalid email id / password");
						$('#errr_login').show();
						$('#errr_login').fadeOut(4000);
						
						}
						else if(doutput == "deactive")
                        {						
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login").html("Your account has been deactivated, Please contact support@bitunio.com");
						$('#errr_login').show();
						$('#errr_login').fadeOut(4000);
						}
						else if(doutput == "change_date")
                        {
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login").html("Your Password is Expired. To proceed with your account, Kindly reset the password");
						$('#errr_login').show();
						$('#errr_login').fadeOut(4000);
						}
						else if(doutput == "emailcheck")
                        {
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login").html("Please Check your mail and activate your Account");
						$('#errr_login').show();
						$('#errr_login').fadeOut(4000);
						}
						else if(doutput == "enable")
                        {
						$('#login').modal('hide');						
						$('#login1').modal('hide');
						$('#tfa').modal('show');
						}
						else 
						{
						if(doutput1 == "You have")
						{
						$('#login_btn').attr('disabled', false);
						$("#msg_errr_login").html(doutput);
						$('#errr_login').show();
						$('#errr_login').fadeOut(4000);
						}
						}
                     }
                 });
				
         },
         
			
			});
			
			
$("#forgot_form").validate({
			rules: {
				email:{required:true,email:true},	
			},
			messages: {
				
			},
			
			submitHandler: function(form) {
           var dataform=$('#forgot_form').serialize();
		   
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/forgotpasswords',
					 beforeSend:function(){
						$('.pageloadingBG, .pageloading').css('display', '');
						$('#forgot_btn').attr('disabled', true);
						},
                    success:function(output) 
                    {
					$('.pageloadingBG, .pageloading').css('display', 'none');
                        var doutput = output.trim();
						
						if(doutput == "success")
                        {
						$('#forgot_btn').attr('disabled', false);						
						$("#forg_mail").val('');
						$("#succc_forgot").show();
						$("#msg_succc_forgot").html("Please check your email and reset your password");
						$('#succc_forgot').fadeOut(4000);
						window.setTimeout(function(){
                         $("#forgot").modal('hide');
						 
						 },3000);

                         }
						 else
                        {
						$('#forgot_btn').attr('disabled', false);
						$("#errr_forgot").show();
						$("#msg_errr_forgot").html("Email does not exist please enter valid email");
						$('#errr_forgot').fadeOut(4000);
						}
						}
                 });
         },
		 });
		 
$("#reset_form").validate({
			rules: {
				
				npassword:{required:true,Passwordconditions:true},
				rpassword:{required:true,equalTo:'#123'},
					},
			messages: {
				npassword:{required:'Required'},
				rpassword:{required:'Required',equalTo:'Do not Match'},
				
					},
					
					submitHandler: function(form) {
           var dataform=$('#reset_form').serialize();
		   
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/reset_password',
					 beforeSend:function(){
						$('.pageloadingBG, .pageloading').css('display', '');
						$('#reset_btn').attr('disabled', true);
						
						},
                     success:function(output)
                     {
					 $('.pageloadingBG, .pageloading').css('display', 'none');
						$('#reset_loader').hide();
                        var doutput = output.trim();
						if(doutput == "success")
                        {
						$('#succc_reset').show();						
						$("#msg_succc_reset").html("Your password has changed successfully, Your password will Expiry in "+pass_ex+" days");
						$('#succc_reset').fadeOut(4000);	
						window.setTimeout(function(){
                        window.location.href = base_url;},4000);
						}
						else
                        {
						$('#errr_reset').show();						
						$("#msg_errr_reset").html("Some error Please try again");
						$('#errr_reset').fadeOut(4000);	                       
						 }
						
						 
						 
						
						
                     }
                 });
				
         },
			
			});			
			
function change_type(type)
{
if(type == "individual")
{
$("#trm").html('<div class="checkbox"><label><input type="checkbox" value="1" name="check"><span class="cr"><i class="cr-icon fa fa-check"></i></span>i agree the <a href="'+base_url+'terms" target="_blank">Terms & conditions</a></label></div>');
}
else 
{
$("#trm").html('<div class="checkbox"><label><input type="checkbox" value="1" name="check"><span class="cr"><i class="cr-icon fa fa-check"></i></span>Agree the <a href="'+base_url+'terms" target="_blank">terms & conditions</a></label></div><div class="checkbox"><label><input type="checkbox" value="1" name="check1"><span class="cr"><i class="cr-icon fa fa-check"></i></span>Agree the <a href="'+base_url+'broker_terms" target="_blank">Affiliate terms & conditions</a></label></div>');
}
}

$("#tfa_form").validate({
			rules: {
				onecode:{required:true,number:true,},	
			
			},
			messages: {
				
			},
			
			submitHandler: function(form) {
           var dataform=$('#tfa_form').serialize();
		   var email=$("#email").val();
		   var clientid=$("#clientid").val();
		   
		   $.ajax({
                     type:'POST',
                     data:dataform+"&email="+email+"&clientid="+clientid,
                     url:base_url+'bitunio/tfa_confirm',
					 beforeSend:function(){
					$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
						$('.pageloadingBG, .pageloading').css('display', 'none');
                        var doutput = output.trim();
						
						if(doutput == "success")
                        {
						window.location.href='dashboard';
                         }
						else
                        {
						$('#tfa_error').show();						
						$("#tfa_error_msg").html("Invalid TFA Code");
						$('#tfa_error').fadeOut(4000);
						$('#tfa_btn').attr('disabled', false);
						        
						}
						
						 
						 
						
						
                     }
                 });
				
         },
		 });
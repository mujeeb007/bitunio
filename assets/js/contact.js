	$("#contact_form").validate({
			rules: {				
				
				contact_name:{required:true},
				contact_message:{required:true},
				subject:{required:true},
				contact_email:{required:true,email:true,},
				},
			messages: {
			},
					
					submitHandler: function(form) {
           var dataform=$('#contact_form').serialize();
		   $.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'bitunio/enquiry',
					 beforeSend:function(){
						
						  $('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					   $('.pageloadingBG, .pageloading').css('display', 'none');
					   $('#contact_form')[0].reset();
					  
						$('#phone_loader').hide();
                        var doutput = output.trim();
						
						if(doutput == "success")
                        {
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Message has been Sent");
						$('#success-modal').modal('show');
						
						}
						else
                        {
						$('#phone_btn').attr('disabled', false);
						$("#stat").html("Error");
						$("#msg").html("Message not Sent");
						$('#error-modal').modal('show');
						}
						}
                 });
				
         },
			
			});
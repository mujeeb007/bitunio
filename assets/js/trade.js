
if(secondCurrency == "INR" ||  secondCurrency == "AED")
{
 var sec_cur_dec=2;
	  var fir_cur_dec=8;
}	  
else 
{
	  var sec_cur_dec=8;
	  var fir_cur_dec=8;
}

var balances='';
$( "#buy_instant_btn" ).click(function() {

var amnt=$("#buy_instant_amnt").val();


if((!isNaN(amnt)) && (amnt != ""))
{
		var f_balance=parseFloat($("#fc_balance").html());
		var s_balance=parseFloat($("#sc_balance").html());
		var tot = parseFloat(amnt) * parseFloat(instant_buy_price);			
		var fee=parseFloat(tot) * parseFloat(comm)/100;
		var total=parseFloat(tot) + parseFloat(fee);

 if(s_balance < total)
{
$("#buy_instant_error").show();
$("#buy_instant_error").html("<center>Insufficient Balance</center>");
return false;
}
if(tot <= 0)
{
$("#buy_instant_error").show();
$("#buy_instant_error").html("<center>Incorrect Amount</center>");
return false;
}
if(amnt < 0.05)
{
$("#buy_instant_error").show();
$("#buy_instant_error").html("<center>Please Enter Above 0.05 </center>");
return false;
}
var dataform=$('#buy_instant').serialize();
$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createbuyorder',
					 beforeSend:function(){
						$("#buy_instant_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						
						}
						else if(doutput == "balance")
						{
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
						$("#stat").html("Error");
						$("#msg").html("Please Enter Valid Values");
						$('#error-modal').modal('show');						
						}
						else
                        {						
						$("#buy_l_subtot").html("0.000");
						$('#buy_instant')[0].reset();
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						mapping(doutput);
						}
						}
			});
			
			
			}
			else 
			{
			$("#buy_instant_error").show();
			$("#buy_instant_error").html("<center>Please Enter Valid Numeric Values</center>");
			}
														});


$( "#buy_limit_btn" ).click(function() {

var amnt=$("#buy_limit_amnt").val();
var price=$("#buy_limit_price").val();

if((!isNaN(price)) && (price != "") && (!isNaN(amnt)) && (amnt != ""))
{
		var f_balance=parseFloat($("#fc_balance").html());
		var s_balance=parseFloat($("#sc_balance").html());
		var tot = parseFloat(amnt) * parseFloat(price);			
		var fee=parseFloat(tot) * parseFloat(comm) /100;			
		var total=parseFloat(tot) + parseFloat(fee);


// if(parseFloat(price) < parseFloat(instant_price))
// {
// $("#buy_limit_error").show();
// $("#buy_limit_error").html("<center>Please Enter Above Current rate</center>");
// return false;
// }
// else

if(s_balance < total)
{
$("#buy_limit_error").show();
$("#buy_limit_error").html("<center>Insufficient Balance</center>");
return false;
}
if(tot <= 0)
{
$("#buy_limit_error").show();
$("#buy_limit_error").html("<center>Incorrect Amount</center>");
return false;
}
if(amnt < 0.05)
{
$("#buy_limit_error").show();
$("#buy_limit_error").html("<center>please Enter Above 0.05 </center>");
return false;
}


var dataform=$('#buy_limit').serialize();
$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createbuyorder',
					 beforeSend:function(){
					 $("#buy_limit_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						
						}
						else if(doutput == "balance")
						{
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
						$("#stat").html("Error");
						$("#msg").html("Please Enter Valid Numeric Values");
						$('#error-modal').modal('show');						
						}
						else
                        {
						$("#buy_limit_subtot").html("0.000");
						$('#buy_limit')[0].reset();						
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						mapping(doutput);
						}
						}
			});
			
			
			}
			else 
			{
				$("#buy_limit_error").show();
				$("#buy_limit_error").html("<center>Please Enter Valid Number Values</center>");
				return false;
			}
														});
														
														
$( "#buy_stop_btn" ).click(function() {

var amnt=$("#buy_stop_amnt").val();
var price=$("#buy_stop_price").val();

if((!isNaN(price)) && (price != ""))
{
		var f_balance=parseFloat($("#fc_balance").html());
		var s_balance=parseFloat($("#sc_balance").html());
		var tot = parseFloat(amnt) * parseFloat(price);			
		var fee=parseFloat(tot) * parseFloat(comm) /100;			
		var total=parseFloat(tot) + parseFloat(fee);


if(parseFloat(price) <= parseFloat(instant_buy_price))
{
$("#buy_stop_error").show();
$("#buy_stop_error").html("<center>Please Enter Above Current rate</center>");
return false;
}
else if(s_balance < total)
{
$("#buy_stop_error").show();
$("#buy_stop_error").html("<center>Insufficient Balance</center>");
return false;
}
else if(tot <= 0)
{
$("#buy_stop_error").show();
$("#buy_stop_error").html("<center>Incorrect Amount</center>");
return false;
}
else if(amnt < 0.05)
{
$("#buy_stop_error").show();
$("#buy_stop_error").html("<center>please Enter Above 0.05 </center>");
return false;
}
//262



var dataform=$('#buy_stop').serialize();
$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createbuyorder',
					 beforeSend:function(){
					 $("#buy_stop_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						
						}
						else if(doutput == "balance")
						{
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
						$("#stat").html("Error");
						$("#msg").html("Please Enter Valid Values");
						$('#error-modal').modal('show');						
						}
						else
                        {
						$("#buy_stop_subtot").html("0.000");
						$('#buy_stop')[0].reset();
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						mapping(doutput);
						}
						}
			});
			}
			else 
			{
			$("#buy_stop_error").html("<center>Please Enter Valid Numeric Values</center>");
			}
			});
			
			
			function buy_l_input(amnt)
			{
			if(amnt != "")
			{
			//alert(instant_price);
			var tot = parseFloat(amnt) * parseFloat(instant_buy_price);
			var fee=parseFloat(tot) * parseFloat(comm) /100;			
			var total=parseFloat(tot) + parseFloat(fee);
			$("#buy_l_subtot").html(total.toFixed(sec_cur_dec));
			}
			else 
			{
			$("#buy_l_subtot").html("0.000");
			}
			
			}
			
			function buy_limit_input()
			{
			
			var amnt=$("#buy_limit_amnt").val();
			var price=$("#buy_limit_price").val();
			if(amnt != "" && price != "")
			{
			//alert(instant_price);
			var tot = parseFloat(amnt) * parseFloat(price);			
			var fee=parseFloat(tot) * parseFloat(comm) /100;			
			var total=parseFloat(tot) + parseFloat(fee);
			$("#buy_limit_subtot").html(total.toFixed(sec_cur_dec));
			}
			else 
			{
			$("#buy_limit_subtot").html("0.000");
			}
			
			}
			function buy_stop_input()
			{
			
			var amnt=$("#buy_stop_amnt").val();
			var price=$("#buy_stop_price").val();
			if(amnt != "" && price != "")
			{
			//alert(instant_price);
			var tot = parseFloat(amnt) * parseFloat(price);			
			var fee=parseFloat(tot) * parseFloat(comm) /100;			
			var total=parseFloat(tot) + parseFloat(fee);
			$("#buy_stop_subtot").html(total.toFixed(sec_cur_dec));
			}
			else 
			{
			$("#buy_stop_subtot").html("0.000");
			}
			
			}
			
			
			
			//Sell buy copy
			
			
			
			
$( "#sell_instant_btn" ).click(function() {

var amnt=$("#sell_instant_amnt").val();


if((!isNaN(amnt)) && (amnt != ""))
{
		var f_balance=parseFloat($("#fc_balance").html());		
		var tot = parseFloat(amnt) * parseFloat(instant_sell_price);			
		var fee=parseFloat(tot) * parseFloat(comm) /100;			
		var total=parseFloat(tot) + parseFloat(fee);

		
 if(f_balance < amnt)
{
$("#sell_instant_error").show();
$("#sell_instant_error").html("<center>Insufficient Balance</center>");
return false;
}
else if(amnt <= 0)
{
$("#sell_instant_error").show();
$("#sell_instant_error").html("<center>Incorrect Amount</center>");
return false;
}
else if(amnt < 0.05)
{
$("#sell_instant_error").show();
$("#sell_instant_error").html("<center>please Enter Above 0.05 </center>");
return false;
}

var dataform=$('#sell_instant').serialize();
$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createsellorder',
					 beforeSend:function(){
						$("#sell_instant_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						
						}
						else if(doutput == "balance")
						{
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
						$("#stat").html("Error");
						$("#msg").html("Please Enter Valid Values");
						$('#error-modal').modal('show');						
						}
						else
                        {	
						$("#sell_l_subtot").html("0.000");
						$('#sell_instant')[0].reset();
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						mapping(doutput);
						}
						}
			});
			
			
			}
			else 
			{
			$("#sell_instant_error").show();
			$("#sell_instant_error").html("<center>Please Enter Valid Numeric Values</center>");
			}
														});


$( "#sell_limit_btn" ).click(function() {

var amnt=$("#sell_limit_amnt").val();
var price=$("#sell_limit_price").val();

if((!isNaN(price)) && (price != "") && (!isNaN(amnt)) && (amnt != ""))
{
		var f_balance=parseFloat($("#fc_balance").html());
		var tot = parseFloat(amnt) * parseFloat(price);			
		var fee=parseFloat(tot) * parseFloat(comm) /100;			
		var total=parseFloat(tot) + parseFloat(fee);

		//alert(amnt); return false;

// if(parseFloat(price) < parseFloat(instant_price))
// {
// $("#sell_limit_error").show();
// $("#sell_limit_error").html("<center>Please Enter Above Current rate</center>");
// return false;
// }
// else 
if(f_balance < amnt)
{
$("#sell_limit_error").show();
$("#sell_limit_error").html("<center>Insufficient Balance</center>");
return false;
}
else if(tot <= 0)
{
$("#sell_limit_error").show();
$("#sell_limit_error").html("<center>Incorrect Amount</center>");
return false;
}
else if(amnt < 0.05)
{
$("#sell_limit_error").show();
$("#sell_limit_error").html("<center>please Enter Above 0.05 </center>");
return false;
}


var dataform=$('#sell_limit').serialize();
$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createsellorder',
					 beforeSend:function(){
					 $("#sell_limit_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						
						}
						else if(doutput == "balance")
						{
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
						$("#stat").html("Error");
						$("#msg").html("Please Enter Valid Numeric Values");
						$('#error-modal').modal('show');						
						}
						else
                        {		
						$("#sell_limit_subtot").html("0.000");
						$('#sell_limit')[0].reset();
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						mapping(doutput);
						}
						}
			});
			
			
			}
			else 
			{
				$("#sell_limit_error").show();
				$("#sell_limit_error").html("<center>Please Enter Valid Number Values</center>");
				return false;
			}
														});
														
														
$( "#sell_stop_btn" ).click(function() {

var amnt=$("#sell_stop_amnt").val();
var price=$("#sell_stop_price").val();

if((!isNaN(price)) && (price != ""))
{
		var f_balance=parseFloat($("#fc_balance").html());
		var tot = parseFloat(amnt) * parseFloat(price);			
		var fee=parseFloat(tot) * parseFloat(comm) /100;			
		var total=parseFloat(tot) + parseFloat(fee);

if(parseFloat(price) >= parseFloat(instant_sell_price))
{
$("#sell_stop_error").show();
$("#sell_stop_error").html("<center>Please Enter Below Current rate</center>");
return false;
}
else if(f_balance < amnt)
{
$("#sell_stop_error").show();
$("#sell_stop_error").html("<center>Insufficient Balance</center>");
return false;
}
else if(tot <= 0)
{
$("#sell_stop_error").show();
$("#sell_stop_error").html("<center>Incorrect Amount</center>");
return false;
}
else if(amnt < 0.05)
{
$("#sell_stop_error").show();
$("#sell_stop_error").html("<center>please Enter Above 0.05 </center>");
return false;
}



var dataform=$('#sell_stop').serialize();
$.ajax({
                     type:'POST',
                     data:dataform,
                     url:base_url+'createsellorder',
					 beforeSend:function(){
					 $("#sell_stop_error").hide();
						$('.pageloadingBG, .pageloading').css('display', '');
						},
                     success:function(output) 
                     {
					
					 	$('.pageloadingBG, .pageloading').css('display', 'none');
					
                        var doutput = output.trim();
						
						if(doutput == "true")
                        {
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						
						}
						else if(doutput == "balance")
						{
						$("#stat").html("Error");
						$("#msg").html("Insufficient Balance");
						$('#error-modal').modal('show');						
						}
						else if(doutput == "empty_value")
						{
						$("#stat").html("Error");
						$("#msg").html("Please Enter Valid Values");
						$('#error-modal').modal('show');						
						}
						else
                        {	
						$("#sell_stop_subtot").html("0.000");
						$('#sell_stop')[0].reset();
						$("#stat_suc").html("Success");
						$("#msg_suc").html("Your Order Has been Placed");
						$('#success-modal').modal('show');
						mapping(doutput);
						}
						}
			});
			}
			else 
			{
			$("#sell_stop_error").show();
			$("#sell_stop_error").html("<center>Please Enter Valid Numeric Values</center>");
			}
			});
			
			
			function sell_l_input(amnt)
			{
			if(amnt != "")
			{
			//alert(instant_price);
			var tot = parseFloat(amnt) * parseFloat(instant_sell_price);
			var fee=parseFloat(tot) * parseFloat(comm) /100;			
			var total=parseFloat(tot) - parseFloat(fee);
			$("#sell_l_subtot").html(total.toFixed(sec_cur_dec));
			
			}
			else 
			{
			$("#sell_l_subtot").html("0.000");
			}
			
			}
			
			function sell_limit_input()
			{
			
			var amnt=$("#sell_limit_amnt").val();
			var price=$("#sell_limit_price").val();
			if(amnt != "" && price != "")
			{
			//alert(instant_price);
			var tot = parseFloat(amnt) * parseFloat(price);			
			var fee=parseFloat(tot) * parseFloat(comm) /100;			
			var total=parseFloat(tot) - parseFloat(fee);
			$("#sell_limit_subtot").html(total.toFixed(sec_cur_dec));
			}
			else 
			{
			$("#sell_limit_subtot").html("0.000");
			}
			
			}
			function sell_stop_input()
			{
			
			var amnt=$("#sell_stop_amnt").val();
			var price=$("#sell_stop_price").val();
			if(amnt != "" && price != "")
			{
			//alert(instant_price);
			var tot = parseFloat(amnt) * parseFloat(price);			
			var fee=parseFloat(tot) * parseFloat(comm) /100;			
			var total=parseFloat(tot) - parseFloat(fee);
			$("#sell_stop_subtot").html(total.toFixed(sec_cur_dec));
			}
			else 
			{
			$("#sell_stop_subtot").html("0.000");
			}
			
			}
			
			
			
			//common
function mapping(id)
{   	
   var cs_name = $('input[name="cs_name"]').val();
     $.ajax({
    url: base_url+'mapping',
    data: "freshorderid="+id+"&cs_name="+cs_name+"&firstCurrency="+firstCurrency+'&secondCurrency='+secondCurrency,
    type: 'POST',            
    success: function(res) 
    {
	//alert(res);
	// if(res == "failure")
	// {
	// successfully placed
	// }
	// else 
	// {
	//successfully completed
	// }
	
	}
    });
}


 setInterval(function(){     
    refreshsellbuyorders(firstCurrency, secondCurrency);
  }, 3000);

function refreshsellbuyorders(pair1,pair2)
  {
	var selected = pair1+'_'+pair2;   
    //var csrf_test_name = $('input[name="csrf_test_name"]').val();
    $.get(base_url+'refreshsellbuyorders/'+pair1+'/'+pair2, function(data){ 
      var sell_orders        = data.sell_orders;
      var buy_orders         = data.buy_orders;
      var stop_orders         = data.stop_orders;
      var trade_history      = data.trade_history;
      var sell_summary       = data.sell_summary;
      var buy_summary        = data.buy_summary;
	  
	  
      var pair_buy_orders    = '';
      var pair_sell_orders   = '';
      var pair_stop_orders   = '';
      var pair_active_orders = '';
      var pair_trade_history = '';
      var sec_cur_dec=2;
	  var fir_cur_dec=8;

if(secondCurrency == "INR" ||  secondCurrency == "AED")
{
 var sec_cur_dec=2;
	  var fir_cur_dec=8;
}	  
else 
{
 var sec_cur_dec=8;
	  var fir_cur_dec=8;
}
	
	$("#fc_balance").html(data.balances[firstCurrency]);
	$("#fc_balance").html(parseFloat(data.balances[firstCurrency]).toFixed(fir_cur_dec));
	$("#sc_balance").html(parseFloat(data.balances[secondCurrency]).toFixed(sec_cur_dec));
	$("#buy_rate").html(parseFloat(data.sell_summary.minPrice).toFixed(sec_cur_dec));
	//$("#buy_limit_price").val(data.sell_summary.minPrice);
	$("#sell_rate").html(parseFloat(data.buy_summary.maxPrice).toFixed(sec_cur_dec));
	//$("#sell_limit_price").val(data.buy_summary.maxPrice);
      if(buy_orders.length>0)
      {
		var buy_title = '';
        for(var i=0;i<buy_orders.length;i++){
		//alert(buy_orders[i].Price); 
          buy_title = 'Total '+pair1+': '+parseFloat(buy_orders[i].amount).toFixed(fir_cur_dec)+', Total '+pair2+': '+((parseFloat(buy_orders[i].Price)*parseFloat(buy_orders[i].amount))).toFixed(sec_cur_dec);
          pair_buy_orders += '<tr title="'+buy_title+'" href="javascript:void(0);" onclick="set_price(\''+buy_orders[i].type+'\',\''+parseFloat(buy_orders[i].amount).toFixed(fir_cur_dec)+'\',\''+parseFloat(buy_orders[i].Price).toFixed(sec_cur_dec)+'\')"><td><span class="green">'+parseFloat(buy_orders[i].Price).toFixed(sec_cur_dec)+'</span></td><td>'+parseFloat(buy_orders[i].amount).toFixed(fir_cur_dec)+'</td><td>'+((parseFloat(buy_orders[i].Price)*parseFloat(buy_orders[i].amount))).toFixed(sec_cur_dec)+'</td></tr>';
        }     
      }
      else
      {
        pair_buy_orders += '<tr><td colspan="3" class="no-records">No orders available</td></tr>';
      }
	  if(sell_orders.length>0)
      {
        // $('.'+pair1+'_'+pair2+'_sell_rate').text(parseFloat(sell_orders[0].price).toFixed(sec_cur_dec));
        // $('#'+pair1+'_'+pair2+' .sell_rate').val(parseFloat(sell_orders[0].price).toFixed(sec_cur_dec));      
        var sell_title = '';
        for(var j=0;j<sell_orders.length;j++){
          sell_title = 'Total '+pair1+': '+parseFloat(sell_orders[j].amount).toFixed(fir_cur_dec)+', Total '+pair2+': '+(parseFloat(sell_orders[j].Price)*parseFloat(sell_orders[j].amount)).toFixed(sec_cur_dec);
          pair_sell_orders += '<tr title="'+sell_title+'" class="text-danger" href="javascript:void(0);" onclick="set_price(\''+sell_orders[j].type+'\',\''+parseFloat(sell_orders[j].amount).toFixed(fir_cur_dec)+'\',\''+parseFloat(sell_orders[j].Price).toFixed(sec_cur_dec)+'\')"><td><span class="text-danger">'+parseFloat(sell_orders[j].Price).toFixed(sec_cur_dec)+'</span></td><td>'+parseFloat(sell_orders[j].amount).toFixed(fir_cur_dec)+'</td><td>'+(parseFloat(sell_orders[j].Price)*parseFloat(sell_orders[j].amount)).toFixed(sec_cur_dec)+'</td></tr>';
        }     
      }
      else
      {
        pair_sell_orders += '<tr><td colspan="3" class="no-records">No orders available</td></tr>';
      }
	  
	  if(stop_orders.length > 0)
      {
        // $('.'+pair1+'_'+pair2+'_sell_rate').text(parseFloat(sell_orders[0].price).toFixed(sec_cur_dec));
        // $('#'+pair1+'_'+pair2+' .sell_rate').val(parseFloat(sell_orders[0].price).toFixed(sec_cur_dec)); active_orders price
        
         for(var js=0;js<stop_orders.length;js++){
          act_fee = parseFloat(stop_orders[js].fee);act_fee_per = parseFloat(stop_orders[js].fee_per);
          cal_fee = parseFloat(stop_orders[js].stoporderprice)*parseFloat(stop_orders[js].amount)*(act_fee_per/100);
          act_tot = (stop_orders[js].Type=='Buy')?(parseFloat(stop_orders[js].stoporderprice)*parseFloat(stop_orders[js].Amount)+parseFloat(stop_orders[js].Fee)):(parseFloat(stop_orders[js].stoporderprice)*parseFloat(stop_orders[js].Amount)-parseFloat(stop_orders[js].Fee));
          pair_stop_orders += '<tr><td>'+stop_orders[js].datetime+'</td><td><span class="'+((stop_orders[js].Type=='Buy')?'green':'red')+'">'+stop_orders[js].Type+'</span></td><td class="sort">'+parseFloat(stop_orders[js].amount).toFixed(fir_cur_dec)+'</td><td>'+parseFloat(stop_orders[js].stoporderprice).toFixed(sec_cur_dec)+'</td><td>'+pair1+"-"+pair2+'</td><td>'+stop_orders[js].Fee+'</td><td>'+act_tot.toFixed(sec_cur_dec)+'</td><td><a href="javascript:void(0);" onclick="cancel_stop_order(\''+stop_orders[js].trade_id+'\',this)">Cancel</a></td>';
        }   
      }
      else
      {
        pair_stop_orders += '<tr><td colspan="8" class="no-records">No orders available</td></tr>';
      }
	  
	  
      
      $('.top_'+pair1+'_'+pair2+' .top_buy_rate').text(to_decimal_with_place(sell_summary.minPrice,sec_cur_dec));
      $('.top_'+pair1+'_'+pair2+' .top_sell_rate').text(to_decimal_with_place(buy_summary.maxPrice,sec_cur_dec));
      $('.top_'+pair1+'_'+pair2+' .top_buy_total').text(to_decimal_with_place(buy_summary.TotalAmount,sec_cur_dec));
      $('.top_'+pair1+'_'+pair2+' .top_sell_total').text(to_decimal_with_place(sell_summary.TotalAmount,sec_cur_dec));

      $('.cur_buy_price').html(to_decimal_with_place(sell_summary.minPrice,sec_cur_dec));
      $('.cur_sell_price').html(to_decimal_with_place(buy_summary.maxPrice,sec_cur_dec));
      
      var active_orders = data.active_orders;
      
      balances[pair1] = data.balances[pair1];
      balances[pair2] = data.balances[pair2];
      $('.'+pair1+'_balance').text(to_decimal_with_place(data.balances[pair1],fir_cur_dec));
      $('.'+pair2+'_balance').text(to_decimal_with_place(data.balances[pair2],sec_cur_dec));

      $('.'+pair1+'_bal').text(to_decimal_with_place(data.balances[pair1],fir_cur_dec));
      $('.'+pair2+'_bal').text(to_decimal_with_place(data.balances[pair2],sec_cur_dec));
      
      if(active_orders.length>8){$('.'+selected+'_history_orders .pair_active_orders').parent().addClass('custome');}
      else{$('#'+selected+'_history_orders .pair_active_orders').parent().removeClass('custome');}

      if(active_orders.length>0){
        var act_tot = '',act_fee = '';
        for(var ka=0;ka<active_orders.length;ka++){
          act_fee = parseFloat(active_orders[ka].fee);act_fee_per = parseFloat(active_orders[ka].fee_per);
          cal_fee = parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].amount)*(act_fee_per/100);
          act_tot = (active_orders[ka].Type=='Buy')?(parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].Amount)+parseFloat(active_orders[ka].Fee)):(parseFloat(active_orders[ka].Price)*parseFloat(active_orders[ka].Amount)-parseFloat(active_orders[ka].Fee));
          pair_active_orders += '<tr><td>'+active_orders[ka].datetime+'</td><td><span class="'+((active_orders[ka].Type=='Buy')?'green':'red')+'">'+active_orders[ka].Type+'</span></td><td class="sort">'+parseFloat(active_orders[ka].amount).toFixed(fir_cur_dec)+'</td><td>'+parseFloat(active_orders[ka].Price).toFixed(sec_cur_dec)+'</td><td>'+pair1+"-"+pair2+'</td><td>'+active_orders[ka].Fee+'</td><td>'+act_tot.toFixed(sec_cur_dec)+'</td><td><a href="javascript:void(0);" onclick="close_active_order(\''+active_orders[ka].trade_id+'\',this)">Cancel</a></td>';
        }     
      }else{
        pair_active_orders += '<tr><td colspan="8" class="no-records">No orders available</td></tr>';
      } 
      
      /*if(trade_history.length>8){$('#'+selected+'_history_orders .pair_trade_history').parent().addClass('custome');}
      else{$('#'+selected+'_history_orders .pair_trade_history').parent().removeClass('custome');}*/

      if(trade_history.length>0)
      {
        var tot = '';
        for(var k=0;k<trade_history.length;k++){
          tot = (trade_history[k].Type=='Buy')?(parseFloat(trade_history[k].Price)*parseFloat(trade_history[k].Amount)).toFixed(sec_cur_dec):(parseFloat(trade_history[k].Price)*parseFloat(trade_history[k].Amount)).toFixed(sec_cur_dec);
          pair_trade_history += '<tr><td><span class="'+((trade_history[k].Type=='Buy')?'green':'red')+'">'+trade_history[k].Price+'</span></td><td>'+trade_history[k].Amount+'</td><td>'+tot+'</td><td>'+trade_history[k].orderDate +' '+trade_history[k].orderTime+'</td>';
        } 
      }
      else
      {
        pair_trade_history += '<tr><td colspan="4" class="no-records">No Trades available</td></tr>';
      }

      //alert(pair_active_orders);
      $('#'+selected+'_buy_orders tbody').html(pair_buy_orders);
      $('#'+selected+'_sell_orders tbody').html(pair_sell_orders);
      $('#'+selected+'_stop_orders tbody').html(pair_stop_orders);
      $('#'+selected+'_history_active_orders tbody').html(pair_active_orders);//.sort('pair_active_orders','desc');
      //$('#'+selected+'_history_orders .pair_trade_history tbody').html(pair_trade_history);
      $('#'+selected+'_history_orders tbody').html(pair_trade_history);
    },'json');
  } 






// setInterval(function(){

 // var data = $('#load_form').serialize();
 // $.ajax ({
		// type:"POST",
		// //dataType: 'json',
		// url:base_url+'load_form',
		// data:data,
		// success:function(output){
		
		// var array = output.split('######');
		// $("#fc_balance").html(array[1]);
		// $("#sc_balance").html(array[0]);
		// $("#scrollDivContent").html(array[2]);
		// $("#scrollDivContent1").html(array[3]);
		// $("#scrollDivContent3").html(array[4]);
		// $("#scrollDivContent2").html(array[5]);
		// $("#scrollDivContent4").html(array[6]);
		// $("#load_btc_twd").html(array[7]);
		// $("#load_eth_twd").html(array[8]);
		// $("#load_btc_eth").html(array[9]);
		// }
		// });  

 // }, 2000);
 
 
 function close_active_order(id, obj){
	if(confirm('Are you sure you want to cancel this?')){
		$(obj).parent().parent().css('opacity', '0.2');
		$.get(base_url+'close_active_order/'+id, function(){
			$(obj).parent().parent().remove();
		
		});
	}
}

//cancel_stop_order
function cancel_stop_order(id, obj){
	if(confirm('Are you sure you want to cancel this order?')){
		$(obj).parent().parent().css('opacity', '0.2');
	$.get(base_url+'cancel_stop_order/'+id, function(){
			$(obj).parent().parent().remove();
			refreshpairorderslist();
		});
	}
}



 function draw_chart_data(firstCurrency,secondCurrency, divID, segment){
		
		$.getJSON(base_url+'bitunio/chart_data/'+firstCurrency+'/'+secondCurrency, function (data) {
		//console.log(data);
	    var dataLength = data.length, i = 0;  
		// split the data set into ohlc and volume


        var newData = [];
        for (i; i < dataLength; i += 1) {          
             newData.push([
               (parseInt(data[i].Timeinteger)*1000), // the date
                parseFloat(data[i].firstPrice), // open
            ]); 
        }
        
        
        $('#'+divID).highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: firstCurrency+' to '+secondCurrency+' exchange rate over time'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Exchange rate'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: firstCurrency+' to '+secondCurrency,
                data: newData
            }]
        });
        if(segment == '1')
		$('#'+divID).removeClass('active');
	});	
		
	}		
	
	draw_chart_data(firstCurrency,secondCurrency, 'chart_div', 1);
	
	function to_decimal_with_place(value,places){

    if(value=='' || value == null)
    return 0;
    else if(parseFloat(value)==0)
    return 0;
    if(parseFloat(value)==parseInt(value))
    return parseInt(value);   
    else{   
      value = parseFloat(value).toFixed(places);
      return parseFloat(value);
    }
  }
  function get_time_string(time_stamp){
    w = new Date(time_stamp * 1000);
    return w.getDate() + '-' + (w.getMonth()+1) + '-' + w.getFullYear() + ' ' + w.getHours() + ':' + w.getMinutes() + ':' + w.getSeconds();
  }
  
  $('.number-8').keypress(function(event) {
      var $this = $(this);
      if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
         ((event.which < 48 || event.which > 57) &&
         (event.which != 0 && event.which != 8))) {
             event.preventDefault();
      }

      var text = $(this).val();
      if ((event.which == 46) && (text.indexOf('.') == -1)) {
          setTimeout(function() {
              if ($this.val().substring($this.val().indexOf('.')).length > 9) {
                  $this.val($this.val().substring(0, $this.val().indexOf('.') + 9));
              }
          }, 1);
      }

      if ((text.indexOf('.') != -1) &&
          (text.substring(text.indexOf('.')).length > 8) &&
          (event.which != 0 && event.which != 8) &&
          ($(this)[0].selectionStart >= text.length - 8)) {
              event.preventDefault();
      }      
  });

  $('.number-8').bind("paste", function(e) {
    var text = e.originalEvent.clipboardData.getData('Text');
    if ($.isNumeric(text)) {
        if ((text.substring(text.indexOf('.')).length > 9) && (text.indexOf('.') > -1)) {
            e.preventDefault();
            $(this).val(text.substring(0, text.indexOf('.') + 9));
       }
    }
    else {
            e.preventDefault();
         }
  });

  $('.number-2').keypress(function(event) {
      var $this = $(this);
      if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
         ((event.which < 48 || event.which > 57) &&
         (event.which != 0 && event.which != 8))) {
             event.preventDefault();
      }

      var text = $(this).val();
      if ((event.which == 46) && (text.indexOf('.') == -1)) {
          setTimeout(function() {
              if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                  $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
              }
          }, 1);
      }

      if ((text.indexOf('.') != -1) &&
          (text.substring(text.indexOf('.')).length > 2) &&
          (event.which != 0 && event.which != 8) &&
          ($(this)[0].selectionStart >= text.length - 2)) {
              event.preventDefault();
      }      
  });

  $('.number-2').bind("paste", function(e) {
    var text = e.originalEvent.clipboardData.getData('Text');
    if ($.isNumeric(text)) {alert(text);
        if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
            e.preventDefault();
            $(this).val(text.substring(0, text.indexOf('.') + 3));
       }
    }
    else {alert('text');
            e.preventDefault();
         }
  });

$("#verify_form").validate({
			rules: {				
				
				name:{required:true},
				id_number:{required:true},
				document1:{required:true},		
				document2:{required:true},		
				document3:{required:true},		
							
				},
			messages: {
				
				
					},
					
submitHandler: function (form) {

                    var data = $('#verify_form').serialize();
                      $.ajax({
                        type:'POST',
                        data:new FormData($('#verify_form')[0]),
						url:base_url+'verification/verification_complete',
						
                        processData: false,
                        contentType: false,
                        success:function(output) {
						$('.pageloadingBG, .pageloading').css('display', 'none');
	                    
						if(output=="success")
							{
								$("#stat_suc").html("Success");
								$("#msg_suc").html("You Have updated your verification details Successfully please wait until admin verify your details");
								$('#success-modal').modal('show');
								setTimeout(function(){ window.location.href=""; }, 4000);	
							  
							}
							else
							{
								
								$("#stat").html("Error");
								$("#msg").html(output);
								$('#error-modal').modal('show');
							}
                        },
                        beforeSend:function()
						{
                         $('.pageloadingBG, .pageloading').css('display', '');
                        }
                      });
                      return false;
                  },
			
			});